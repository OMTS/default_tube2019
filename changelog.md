# 6.2.2
- Updated entire default_tube2019 template
- Fixed issue with uploader in default_tube2019
- Fixed filters for default_tube2019
