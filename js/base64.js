jQuery.base64 = function (a) {
    function b(a, b) {
        var c = g.indexOf(a.charAt(b));
        if (-1 === c)
            throw "Cannot decode base64";
        return c;
    }
    function c(a) {
        var c, d, e = 0, g = a.length, h = [];
        if (a = String(a), 0 === g)
            return a;
        if (g % 4 !== 0)
            throw "Cannot decode base64";
        for (a.charAt(g - 1) === f && (e = 1, a.charAt(g - 2) === f && (e = 2), g -= 4),
                c = 0; g > c; c += 4)
            d = b(a, c) << 18 | b(a, c + 1) << 12 | b(a, c + 2) << 6 | b(a, c + 3),
                    h.push(String.fromCharCode(d >> 16, d >> 8 & 255, 255 & d));
        switch (e) {
            case 1:
                d = b(a, c) << 18 | b(a, c + 1) << 12 | b(a, c + 2) << 6, h.push(String.fromCharCode(d >> 16, d >> 8 & 255));
                break;

            case 2:
                d = b(a, c) << 18 | b(a, c + 1) << 12, h.push(String.fromCharCode(d >> 16));
        }
        return h.join("");
    }
    function d(a, b) {
        var c = a.charCodeAt(b);
        if (c > 255)
            throw "INVALID_CHARACTER_ERR: DOM Exception 5";
        return c;
    }
    function e(a) {
        if (1 !== arguments.length)
            throw "SyntaxError: exactly one argument required";
        a = String(a);
        var b, c, e = [], h = a.length - a.length % 3;
        if (0 === a.length)
            return a;
        for (b = 0; h > b; b += 3)
            c = d(a, b) << 16 | d(a, b + 1) << 8 | d(a, b + 2), e.push(g.charAt(c >> 18)),
                    e.push(g.charAt(c >> 12 & 63)), e.push(g.charAt(c >> 6 & 63)), e.push(g.charAt(63 & c));
        switch (a.length - h) {
            case 1:
                c = d(a, b) << 16, e.push(g.charAt(c >> 18) + g.charAt(c >> 12 & 63) + f + f);
                break;

            case 2:
                c = d(a, b) << 16 | d(a, b + 1) << 8, e.push(g.charAt(c >> 18) + g.charAt(c >> 12 & 63) + g.charAt(c >> 6 & 63) + f);
        }
        return e.join("");
    }
    var f = "=", g = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", h = "1.0";
    return {
        decode: c,
        encode: e,
        VERSION: h
    };
}(jQuery)