(function ($) {

    function ww() {
        return $(window).innerWidth();
    }

    function getAttributes(elem) {
        var c = {};
        return $(elem).length && $.each($(elem)[0].attributes, function (a, b) {
            c[b.name] = b.value;
        }), c;
    }

    function searchTrigger() {
        var trigger = $('.btn-search'),
                box = trigger.parents('.search-box');

        function setAction() {
            if (ww() > 1039) {
                trigger.click(function (e) {
                    if (!box.hasClass('active')) {
                        e.preventDefault();
                        box.addClass('active');
                    }
                });

                $(document).on('click', function (e) {
                    var target = e.target;
                    if ((!$(target).is('.search-box') && !$(target).parents('.search-box').length)) {
                        box.removeClass('active');
                    }
                });
            }
        }

        $(document).ready(function () {
            setAction();
        }), $(window).resize(function () {
            setAction();
        });
    }

    function setContentAd() {
        if ($('.item-col.-aff').length > 0) {
            function getHeight(elem) {
                return elem.innerHeight();
            }

            function getItemMargin(elem) {
                return parseInt(elem.css("margin-bottom"));
            }

            function setAction() {
                var ad = $('.item-col.-aff'),
                        item = ad.siblings('.item-col'),
                        ad_height = ad.innerHeight();

                if (ww() > 767) {
                    if (ad_height !== (2 * getHeight(item) + getItemMargin(item))) {
                        ad.height(2 * getHeight(item) + getItemMargin(item) + 1);
                    }
                } else {
                    ad.height('');
                }
            }

            $(window).load(function () {
                setAction();
            }).resize(function () {
                setAction();
            });
        }
    }

    function mobileNavDrops() {
        var trigger = $('.show-drop');

        trigger.click(function (e) {
            e.preventDefault();
            if (ww() < 1040) {
                $(this).parent('.nav-elem').siblings().removeClass('show');
                $(this).parent('.nav-elem').toggleClass('show');
            }
        });
    }

    function mbTriggers() {
        var trigger = $('[data-mb="trigger"]');

        $(document).on('click', function (e) {
            var target = e.target;
            if ($('body').hasClass('nav-open')) {
                if ((!$(target).is('.main-nav') && !$(target).parents('.main-nav').length) && (!$(target).is('.btn-trigger-nav') && !$(target).parents('.btn-trigger-nav').length)) {
                    $('body').removeClass('nav-open');
                }
            }
            if ($('body').hasClass('search-open')) {
                if ((!$(target).is('.search-wrap') && !$(target).parents('.search-wrap').length) && (!$(target).is('.btn-trigger-search') && !$(target).parents('.btn-trigger-search').length)) {
                    $('body').removeClass('search-open');
                }
            }
        });

        trigger.each(function () {
            $(this).click(function (e) {
                e.preventDefault();
                if ($('body').hasClass('[class*="-open"]')) {
                    $('body').removeClass('[class*="-open"]')
                }
                if ($('body').hasClass($(this).data('target') + '-open')) {
                    $('body').removeClass($(this).data('target') + '-open');
                } else {
                    $('body').addClass($(this).data('target') + '-open');
                    if ($(this).data('target') === 'search') {
                        $('.search-wrap').find('input[type="text"]').focus();
                    }
                }
            });
        });
    }

    function embedCode() {
        if ($('.embed-code').length > 0) {
            $('.embed-code').click(function () {
                window.getSelection().removeAllRanges();
                $(this).select();
                document.execCommand("copy");
                $('.copy-info').addClass('open');
                setTimeout(function () {
                    $('.copy-info').removeClass('open');
                }, 2000);
            });
        }
    }

    function expandContent(options) {
        function gatherComments() {
            sett.comments = $(".comments-load .comment-box:gt(" + sett.l + ")").clone(true, true),
                    $(".comments-load .comment-box:gt(" + sett.l + ")").remove(),
                    $(".comments-load").after('<div data-opt-expand-target="comments" class="more-comments"></div>'),
                    $(".more-comments").append(sett.comments),
                    $(".comments-load").not(".more-comments").slideDown(sett.slideTime);
        }
        var defaults = {
            btn: $("[data-mb='expand']"),
            comments: "",
            limitComments: "3",
            slideTime: 1e3,
            l: ""
        },
                sett = $.extend(!0, {}, defaults, options);

        sett.btn.on("click", function (e) {
            e.preventDefault();
            var aa = getAttributes($(this)),
                    target = aa["data-opt-target"];

            $(this).hasClass("active") ? ($(this).removeClass("active"), $("[data-opt-expand-target='" + target + "']").slideUp(sett.slideTime)) : ($(this).addClass("active"), $("[data-opt-expand-target='" + target + "']").slideDown(sett.slideTime));
        });
        $("[data-opt-target='comments']").hide();
        $(window).load(function () {
            sett.l = $("[data-opt-target='comments']").filter("[data-opt-limit]").length ? $("[data-opt-target='comments']").filter("[data-opt-limit]").attr("data-opt-limit") - 1 : sett.limitComments - 1,
                    setTimeout(function () {
                        var comments_count = $(".comments-load .comment-box").length;
                        comments_count > sett.l + 1 && ($("[data-opt-target='comments']").css("display", "inline-block"),
                                gatherComments());
                    }, 1e3);
        });
    }

    function buildNewModal(b, c) {
        var context,
                e = {},
                opt = $.extend(true, {}, e, c),
                html = '<div id="' + opt.id + '-modal" class="modal fade" tabindex="-1" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">' + opt.title + '</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body">' + b + '</div><div class="modal-footer"><button class="btn btn-default" data-dismiss="modal" type="button">' + opt.closeTitle + "</button></div></div></div></div>";

        $("body").append(html),
                "object" == typeof b && $("#" + opt.id + "-modal").find(".modal-body").text("").append(b),
                context = $("#" + opt.id + "-modal"),
                context.modal("show"),
                context.on("hidden.bs.modal", function () {
                    context.remove();
                });
    }
    function modalWindows(d) {
        var e = {
            btn: $("[data-mb='modal']")
        };
        e.btn.on("click", function (b) {
            b.preventDefault();
            var d = getAttributes($(this)),
                    e = d["data-opt-iframe-width"] ? d["data-opt-iframe-width"] : "100%",
                    f = d["data-opt-iframe-height"] ? d["data-opt-iframe-height"] : "150px",
                    g = {
                        rand: Math.floor(1e3 * Math.random(0, 9999)),
                        closeTitle: d["data-opt-close"] ? d["data-opt-close"] : "Close",
                        id: d.id ? d.id : "modal-" + this.rand,
                        title: d.title ? d.title : "Popup"
                    };
            if ("iframe" == d["data-opt-type"]) {
                var h = '<iframe src="' + d.href + '" style="width:' + e + ";height:" + f + ';"></iframe>';
                buildNewModal(h, g);
            } else
                "object" == typeof xhr && xhr.abort(), xhr = $.ajax({
                    url: d.href,
                    type: "GET",
                    dataType: "html",
                    cache: !1,
                    crossDomain: !1,
                    data: {},
                    success: function (b) {
                        var d = $("<div/>").html(b);
                        if (b.match(/<body>/))
                            var e = d.children().not("meta, link");
                        else
                            var e = b;
                        buildNewModal(e, g);
                    },
                    error: function (a) {
                        h = a;
                    }
                });
        });
    }

    function createFilterTrigger() {
        function d(a) {
            a ? m.addClass(n) : m.removeClass(n);
        }
        function e(a) {
            l.before(a);
            l.append(a);
        }
        function f(c) {
            $('[data-mb="' + c + '"]').remove(),
                    l.css("display", ""),
                    $("[data-mb='" + k + "']").removeClass("active"),
                    d(!1);
        }
        function g() {
            $("[data-mb='" + k + "']").on("click", function (c) {
                $(this).hasClass("active") ? ($("[data-mb='" + k + "']").removeClass("active"), l.slideUp(), d(!1)) : ($("[data-mb='" + k + "']").addClass("active"), l.slideDown(), d(!0));
                var scroll = $(".channels-list").tinyscrollbar();
                $(scroll).data().plugin_tinyscrollbar.update();
                setTimeout(function () {
                    $('html, body').animate({scrollTop: (l.offset().top - 50)}, 200);
                }, 200);
            });
        }
        function h() {
            if ($("[data-mb='aside']").length && ww() < 768) {
                var d = getAttributes($('[data-mb="aside"]')),
                        label_off = d["data-opt-filters-on"] ? d["data-opt-filters-on"] : "Show filters",
                        label_on = d["data-opt-filters-off"] ? d["data-opt-filters-off"] : "Hide filters",
                        trigger = '<div data-mb="' + i + '" class="filter-trigger-col col"><button data-mb="' + k + '" class="btn btn-header ' + j + '"><i class="fas fa-sliders-h"></i><span class="sub-label -on">' + label_on + '</span><span class="sub-label -off">' + label_off + "</span></button></div>";
                0 == $("[data-mb='" + k + "']").length && (e(trigger), g());
            } else {
                f(i);
            }
        }
        var i = "aside-filter-trigger",
                j = "show-filters",
                k = "show-aside",
                l = ($("." + j), $("[data-mb='aside']")),
                m = $("body"),
                n = "aside-mobile-open";
        $(window).load(function () {
            h();
        }), $(window).resize(function () {
            h();
        });
    }

    function ajaxGalleryNavigation() {
        var trigger_elem = $('[data-mb="load-img"]'),
                trigger = '[data-mb="load-img"]',
                container = 'data-mb="ajax-container"';

        if (trigger_elem.length > 0) {
            $(window).on("keyup", function (key) {
                switch (key.which) {
                    case 37:
                        $(document).find(trigger).filter('[rel="prev"]').click();
                        break;

                    case 39:
                        $(document).find(trigger).filter('[rel="next"]').click();
                        break;

                    default:
                        return;
                }
                key.preventDefault();
            });

            $(window).on("click", function (e) {
                var src = "";
                if ($(e.target).attr('data-mb') === "load-img" || $(e.target).parent().attr('data-mb') === "load-img") {
                    e.preventDefault();

                    src = $(e.target).attr('data-mb') === "load-img" ? $(e.target).attr("href") : $(e.target).parent().attr("href");
                    $("[" + container + "]").css("min-height", $("[" + container + "] img").height());
                    $("[" + container + "]").addClass("loading-img");

                    var xhr;

                    "object" == typeof xhr && xhr.abort(), xhr = $.ajax({
                        url: src,
                        type: "GET",
                        dataType: "html",
                        cache: false,
                        crossDomain: false,
                        data: {},
                        success: function (b) {
                            var container_elem = $(b).find("[" + container + "]"),
                                    img = $(b).find("[" + container + "] img"),
                                    title = $(b).filter("title").text(),
                                    index = container_elem.attr("data-opt-current");

                            container_elem.addClass("loading-img");

                            var info = getAttributes(".loading-img"),
                                    data = {
                                        rand: Math.floor(1e3 * Math.random(0, 9999)),
                                        closeTitle: info["data-opt-close"] ? info["data-opt-close"] : "Close",
                                        id: info.id ? info.id : "modal-" + this.rand,
                                        title: info["data-opt-title"] ? info["data-opt-title"] : "Error"
                                    };

                            img.on("load", function () {
                                console.log("image loaded correctly");
                                $("[" + container + "]").removeClass("loading-img");
                            });

                            img.on("error", function () {
                                buildNewModal("Error loading image", data);
                            });

                            img.attr("src", img.attr("src")), function (a, b) {
                                History.Adapter.bind(a, "statechange", function () {
                                    History.getState();
                                }), History.pushState({
                                    state: 1
                                }, title, "?image=" + index);
                            }(window);

                            $("[" + container + "]").replaceWith(container_elem);

                            setTimeout(function () {
                                $(".owl-carousel").trigger("to.owl.carousel", [parseInt(index) - 1, 0, !0]),
                                        $(".gallery-item-col").removeClass("active");
                                $(".gi-" + index).addClass("active");
                            }, 500);
                        },
                        error: function (error) {
                            console.log("ERROR " + error);
                        }
                    });
                }
            });
        }
    }

    function inputAutocomplete() {
        $("input#additionalUsername").length && $("input#additionalUsername").autocomplete({
            source: function (b, c) {
                $.getJSON(_basehttp + "/includes/mailbox/mail_autocomplete.php", {
                    term: b.term
                }, c);
            },
            minLength: 3
        });
    }

    function rating() {
        if ($('[data-mb="thumbs-rating"]').length) {
            var _attr,
                    url = _basehttp + "/includes/rating/jRating.php";
            $('[data-mb="vote"]').click(function (b) {
                _attr = getAttributes($(this));
                var j = _attr["data-opt-vote"] === 'up' ? 100 : 0,
                        rating = $(this).parents('[data-mb="thumbs-rating"]'),
                        id = rating.attr("data-opt-id"),
                        type = rating.attr("data-opt-type") ? rating.attr("data-opt-type") : 0;

                b.preventDefault();
                $.post(url, {
                    idBox: id,
                    type: type,
                    rate: j,
                    action: "rating"
                }, function (a) {
                    var data = {
                        rand: Math.floor(1e3 * Math.random(0, 9999)),
                        closeTitle: _attr["data-opt-close"],
                        id: "voting",
                        title: _attr.title
                    };
                    buildNewModal(a.message, data);
                    if (a.success) {
                        $('.rating-current').text(a.newRating + '%');
                        if (_attr["data-opt-vote"] === 'up') {
                            var likes = $('.rating-count .count').text().replace('(', '').replace(')', '');
                            $('.rating-count .count').text('(' + (parseInt(likes) + 1) + ')');
                        }
                    }
                }, "json");
            });
        }
    }

    function popupWindow() {
        function c(a, b, c, d) {
            var e = screen.width / 2 - c / 2, f = screen.height / 2 - d / 2;
            return window.open(a, b, "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=" + c + ", height=" + d + ", top=" + f + ", left=" + e);
        }
        if ($('[data-mb="popup"]').length) {
            var d = $('[data-mb="popup"]');
            d.on("click", function (b) {
                b.preventDefault();
                var d = $(this).attr("href"),
                        e = $(this).attr("title"),
                        f = $(this).attr("data-opt-width"),
                        g = $(this).attr("data-opt-height");
                c(d, e, f, g);
            });
        }
    }

    function comments() {
        var d = {
            comments_container: $('[data-mb="load-comments"]'),
            form: $('[data-mb="add-comment"]'),
            loaded: 0,
            newCommentID: "new-comment",
            bodyNewClass: "comment-added",
            scrollToTimeout: 500,
            scrollToLength: 1e3,
            loadComments: function () {
                if (this.comments_container.length) {
                    var b = getAttributes(this.comments_container),
                            d = b["data-opt-url"] + "?id=" + b["data-opt-id"] + "&type=" + b["data-opt-type"] + "&loaded=" + this.loaded + "&time=" + new Date().getTime();
                    1 == this.loaded ? ($(".comments-load").length ? $(".comments-load").before("<div id='" + this.newCommentID + "' style='display:none;'></div>") : (this.comments_container.children("p").remove().append("<div id='" + this.newCommentID + "' style='display:none;'></div>"),
                            this.comments_container.append("<div id='" + this.newCommentID + "' style='display:none;'></div>")),
                            $("#new-comment").load(d)) : (this.comments_container.load(d), this.loaded = 1);
                }
            },
            addComment: function (c) {
                var d = this, e = this.form.find("button[type='submit']");
                e.on("click", function (c) {
                    c.preventDefault(), $.ajax({
                        type: "POST",
                        dataType: "json",
                        data: $(this.form).serialize(),
                        url: _basehttp + "/includes/ajax.post_comment.php",
                        cache: !1,
                        crossDomain: !1,
                        success: function (c) {
                            "true" !== c.error && ($(d.form).find("textarea").val("").prop("disabled", !0),
                                    $("body").addClass(d.bodyNewClass),
                                    $("body").trigger("added.mb.comment")),
                                    "true" == c.error || d.reloadComments(),
                                    $("[data-mb='comment-alert']").html(c.info),
                                    setTimeout(function () {
                                        $("#" + d.newCommentID).slideDown();
                                    }, d.scrollToTimeout), d.resetCaptha();
                        }
                    });
                });
            },
            resetCaptha: function () {
                var b = Math.floor(1e3 * Math.random() + 1);
                $("img.captcha").attr("src", _basehttp + "/captcha.php?" + b);
            },
            reloadComments: function () {
                this.loadComments();
            }
        };
        d.loadComments(), d.addComment();
    }

    function shuffleThumbs(d) {
        var e, f, g = {
            limit: 10,
            selector: "[data-mb='shuffle-thumbs']",
            timeout: 500
        }, h = $.extend(!0, {}, g, d), i = $(h.selector), j = "";
        i.hover(function () {
            j = $(this).attr("src"), f = $(this);
            var b = getAttributes($(this)), d = b["data-opt-limit"] ? b["data-opt-limit"] : h.limit, g = b["data-opt-timeout"] ? b["data-opt-timeout"] : h.timeout, i = j.lastIndexOf("."), k = j.slice(0, i - 1), l = j.slice(i, j.length), m = parseInt(j.slice(i - 1, i));
            m++, m == d && (m = 1), f.attr("src", k + m + l), e = setInterval(function () {
                m++, m == d && (m = 1), f.attr("src", k + m + l);
            }, g);
        }, function () {
            f.attr("src", j), clearInterval(e);
        });
    }

    function fakeUpload() {
        if ($('[data-mb="fake-upload"]').length > 0) {
            var trigger = $('[data-mb="fake-upload"]'),
                    input = $('[data-opt="' + trigger.attr("data-opt-target") + '"]');

            trigger.on("click", function (a) {
                a.preventDefault(), input.click();
            });
            input.change(function () {
                trigger.find(".sub-label").text($(this).val().replace(/^.*\\/, ""));
            });
        }
    }

    $(document).ready(function () {
        if ($('.-scrollbar').length) {
            $('.-scrollbar').tinyscrollbar();
        }
        if ($('[data-style="btn-selectpicker"]').length) {
            $('[data-style="btn-selectpicker"]').selectpicker({
                dropupAuto: false
            });
        }

        if ($('img.lazy').length) {
            $('img.lazy').lazyload();
        }

        if ($('.gallery-slider').length) {
            $('.gallery-slider').owlCarousel({
                items: 8,
                responsive: {
                    0: {items: 2},
                    480: {items: 3},
                    768: {items: 5},
                    1040: {items: 6},
                    1470: {items: 8}
                },
                nav: true,
                dots: false,
                autoplay: false,
                loop: false,
                navText: ["<span class='slider-prev'><span></span></span>", "<span class='slider-next'><span></span></span>"]
            });
        }

        if ($('#range_length_filter').length) {
            var slider = $('#range_length_filter');
            slider.ionRangeSlider({
                type: "double",
                step: typeof slider.attr('data-step') != "undefined" ? parseInt(slider.attr('data-step')) : 5,
                grid: true,
                grid_snap: true,
                oldUrl: "",
                newUrl: "",
                onStart: function (data) {},
                onChange: function (data) {},
                onFinish: function (data) {
                    var link = window.location.href,
                            d = "",
                            e = (link.length, typeof slider.attr("data-multiplication") != "undefined" ? parseInt(slider.attr("data-multiplication")) : 1),
                            f = data.max * e,
                            g = data.to * e,
                            h = data.from * e,
                            data_to = slider.attr("data-attr-to") + "=" + g,
                            data_from = slider.attr("data-attr-from") + "=" + h,
                            link_split = [],
                            l = [];
                    if (console.log(e), link.indexOf("?") > -1) {
                        link_split = link.split("?");
                        var m = link_split[0];
                        link_split = link_split[1], l = link_split.split("&");
                        for (var n = l.length - 1; n >= 0; n--) {
                            (l[n].indexOf(slider.attr("data-attr-to")) > -1 || l[n].indexOf(slider.attr("data-attr-from")) > -1) && l.splice(n, 1);
                        }
                    } else {
                        var m = link;
                    }
                    h > data.min && l.push(data_from), g != f && l.push(data_to);
                    for (var o = [], p = [], n = 0; n < l.length; n++)
                        p.push(l[n]), o += 0 == n ? "?" + l[n] : "&" + l[n];
                    d = m + o, "onpost" != slider.attr("data-method") && (window.location.href = d);
                },
                onUpdate: function (data) {}
            });
        }

        searchTrigger();
        setContentAd();
        mobileNavDrops();
        mbTriggers();
        if ($('.img2svg').length > 0) {
            $.img2svg('.img2svg');
        }
        embedCode();
        ajaxGalleryNavigation();
        createFilterTrigger();
        popupWindow();
        inputAutocomplete();
        rating();
        expandContent();
        comments();
        modalWindows();
        fakeUpload();
    });

    $(window).load(function () {
        shuffleThumbs();
    });

})(jQuery);