<?php
include_once("../../mb.php");
if (is_numeric($_GET['id'])) {
    $id = $_GET['id'];
}

$type = (int) $_GET['type'];
$loaded = "";
if ($_GET['loaded']) {
    $loaded = " LIMIT 0, 1";
}

$cresult = dbQuery("SELECT *, (SELECT username FROM users WHERE users.record_num = comments.userid) AS username, (SELECT avatar FROM users WHERE users.record_num = comments.userid) AS avatar, (SELECT gender FROM users WHERE users.record_num = comments.userid) AS gender FROM comments WHERE content = '$id' AND type = '" . $type . "' ORDER BY timestamp DESC" . $loaded, false);
if (count($cresult) > 0) {
    ?>
    <?php
    foreach ($cresult as $crow) {
        if (isset($crow['username']) && $crow['username'] != "") {
            $username = $crow['username'];
            $link = generateUrl('user', $crow['username'], $crow['userid']);
        } else {
            $username = _t("Anonymous");
            $link = '';
        }
        ?>
        <div class="comment-box">
            <div class="comment-avatar">
                <?php if ($link) echo '<a href="' . $link . '">'; ?>   
                <?php if ($crow['avatar'] != '' && file_exists("$basepath/media/misc/{$crow['avatar']}")) { ?>
                    <img src='<?php echo $basehttp; ?>/media/misc/<?php echo $crow['avatar']; ?>' alt= '<?php echo ucwords($crow['username']); ?>'>
                <?php } else { ?>
                    <?php if (strtolower($crow['gender']) == 'male') { ?>
                        <img src='<?php echo $basehttp; ?>/core/images/avatar_male.png'  alt= '<?php echo ucwords($crow['username']); ?>'>
                    <?php } elseif (strtolower($crow['gender']) == 'female') { ?>
                        <img src='<?php echo $basehttp; ?>/core/images/avatar_female.png'  alt= '<?php echo ucwords($crow['username']); ?>'>
                    <?php } else { ?>
                        <img src='<?php echo $basehttp; ?>/core/images/avatar_default.png'  alt= '<?php echo ucwords($crow['username']); ?>'>
                    <?php } ?>
                <?php } ?>
                <?php if ($link) echo '</a>'; ?>
            </div>

            <div class="comment-content">
                <div class="comment-info">
                    <div class="comment-submitter">
                        <?php
                        if ($link)
                            echo '<a href="' . $link . '">';
                        echo $username;
                        if ($link)
                            echo '</a>';
                        ?> 
                    </div>

                    <div class="comment-date"><?php echo date('d.m.Y', $crow['timestamp']); ?></div>

                    <div class="comment-time">
                        <?php echo date('H:i', $crow['timestamp']); ?>
                    </div>
                </div>

                <div class="comment-text">
                    <?php echo $crow['comment']; ?>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } else { ?>
    <?php if ($type == 0) { ?>
        <div class="comments-note"><?php echo _t("There are no comments for this video. Please leave your feedback and be the first!") ?></div>
    <?php } elseif ($type == 1) { ?>
        <div class="comments-note"><?php echo _t("There are no comments for this model. Please leave your feedback and be the first!") ?></div>
    <?php } else { ?>
        <div class="comments-note"><?php echo _t("There are no comments for this profile. Please leave your feedback and be the first!") ?></div>
    <?php } ?>
<?php } ?>