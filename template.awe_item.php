<?php
$link =  $basehttp . '/webcam-video/' . clearString($row['title']) . '-' . $row['id'] . '.html';
$dirname = str_replace('.flv', '', $row['orig_filename']);
$subdir = $row['filename'][0] . '/' . $row['filename'][1] . '/' . $row['filename'][2] . '/' . $row['filename'][3] . '/' . $row['filename'][4] . '/';
$dirname = $subdir . $dirname;
$uniq = uniqid();
?>
<!-- item :: column :: start -->

<!-- item -->
<div class="item-col col -video">
    <a href="<?php echo $link; ?>" title="<?php echo htmlentities($row['title'], ENT_QUOTES, 'UTF-8'); ?>">
        <span class="image">
			<img src="<? echo $row['thumbnail']; ?>" alt="<?php echo htmlentities($row['title'], ENT_QUOTES, 'UTF-8'); ?>">
         
            <?php if ($row['movie_height'] >= 720) { ?>
                <span class="item-quality">HD</span>
            <?php } ?>
            <span class="item-time"><?php echo sec2time($row['length']); ?></span>

            <?php if ($row['access_level'] > 0) { ?>
                <span class="item-access">
                    <i class="fas fa-unlock-alt"></i>
                    <span class="sub-desc"><?php
                        if ($row['access_level'] == 1) {
                            echo _t("Private");
                        } else {
                            echo _t("Premium");
                        }
                        ?></span>
                </span>
            <?php } ?>
        </span>

        <span class="item-info">
            <span class="item-name"><?php echo $row['title']; ?></span>
            <span class="item-rate"><?php echo $row['rating']; ?>%</span>
        </span>
    </a>

    <a href="<?php echo $basehttp; ?>/action.php?action=<?php echo ($_GET['mode'] == 'favorites') ? "remove" : "add"; ?>_favorites&id=<?php echo $row['record_num']; ?>" data-mb="modal" data-opt-type="ajax" data-opt-close="<?php echo _t("Close") ?>" data-toggle="tooltip" title="<?php echo ($_GET['mode'] == 'favorites') ? _t("Remove from favorites") : _t("Add to favorites"); ?>" class="add-to-fav">
        <?php if ($_GET['mode'] == 'favorites') { ?>
            <i class="fas fa-minus-circle"></i>
        <?php } else { ?>
            <i class="fas fa-plus-circle"></i>
        <?php } ?>
    </a>
    <?php if ($_SESSION['userid'] && ($row['submitter'] == $_SESSION['userid'])) { ?>
        <a href="<?php echo $basehttp; ?>/edit-content/?id=<?php echo $row['record_num']; ?>" title="<?php echo _t("Edit") ?>" class="edit-content">
            <i class="far fa-edit"></i>
        </a>
    <?php } ?>
</div>
<!-- item END -->

