<?
if (is_array($results)) {
    $i = 0;
    foreach ($results as $row) {
        if ($row['langTitle']) {
            $row['title'] = $row['langTitle'];
        }
        if ($row['langDescription']) {
            $row['description'] = $row['langDescription'];
        }
        if ($row['langKeywords']) {
            $row['keywords'] = $row['langKeywords'];
        }
        if ($results_per_row > 0) {
            $i++;
            $rest = $i % $results_per_row;
            if ($rest == 0) {
                $class = ' last';
            } else {
                $class = '';
            }
        }
        getTemplate("template.awe_item.php");
    }
} else {
    echo setMessage(_t("Sorry, no results were found."), 'alert', true);
}