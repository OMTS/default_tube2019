<div class="form-col -contact col">
    <form class="form-block" id="contact-form"  name="formContact" method="post" action="">
        <div class="row">

            <div class="form-item col -half">
                <input class="form-control" name="name" type="text" id="name" value="<? echo $_SESSION['username']; ?>" placeholder="<?php echo _t("Username") ?>">
            </div>

            <div class="form-item col -half">
                <input class="form-control" name="email" type="text" id="email"  value="" placeholder="<?php echo _t("Email") ?>">
            </div>


        </div>

        <div class="row">

            <div class="form-item col -full">
                <textarea name="message" id="textfield3" placeholder="<?php echo _t("Message") ?>"></textarea>
            </div>


        </div>

        <div class="row">

            <div class="form-item col -captcha">
                <div class="captcha-wrapper">
                    <img src="<?php echo $basehttp; ?>/captcha.php?<?php echo time(); ?>" class="captcha-img">
                    <input class="form-control captcha-input" name="captchaaa" type="text" value="" placeholder="<?php echo _t("Human?") ?>">
                </div>
            </div>


        </div>

        <div class="row">

            <div class="form-item col -actions">
                <button class="btn btn-default" type="submit" name="Submit"><span class="btn-label"><?php echo _t("Send message") ?></span></button>
            </div>


        </div>
    </form>
</div>
