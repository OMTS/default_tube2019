<?php
$link = generateUrl('galleries', $row['title'], $row['record_num']);
$dirname = str_replace('.flv', '', $row['orig_filename']);
$subdir = $row['filename'][0] . '/' . $row['filename'][1] . '/' . $row['filename'][2] . '/' . $row['filename'][3] . '/' . $row['filename'][4] . '/';
$dirname = $subdir . $dirname;
$counter = dbValue("SELECT COUNT(*) AS `count` FROM `images` WHERE `gallery` = '$row[record_num]'", 'count', true);
if (!$row['thumbfile']) {
    $row['thumbfile'] = dbValue("SELECT `filename` FROM `images` WHERE `record_num` = '$row[thumbnail]'", 'filename', true);
}
?>
<!-- item -->
<div class="item-col col -gallery">
    <a href="<?php echo $link; ?>" title="<?php echo htmlentities($row['title'], ENT_QUOTES, 'UTF-8'); ?>">
        <span class="image">
            <?php if ($config['lazyload']) { ?>
                <img class="lazy" src="<?php echo $template_url; ?>/images/loader_vertical.jpg" data-src="<?php echo $gallery_url; ?>/<?php echo $row['filename']; ?>/thumbs/<?php echo $row['thumbfile']; ?>" alt="<?php echo htmlentities($row['title'], ENT_QUOTES, 'UTF-8'); ?>">
            <?php } else { ?>
                <img src="<?php echo $gallery_url; ?>/<?php echo $row['filename']; ?>/thumbs/<?php echo $row['thumbfile']; ?>" alt="<?php echo htmlentities($row['title'], ENT_QUOTES, 'UTF-8'); ?>">
            <?php } ?>
            <span class="item-time">
                <i class="fas fa-image"></i>
                <?php echo $counter; ?>
            </span>

            <?php if ($row['access_level'] > 0) { ?>
                <span class="item-access">
                    <i class="fas fa-unlock-alt"></i>
                    <span class="sub-desc"><?php
                        if ($row['access_level'] == 1) {
                            echo _t("Private");
                        } else {
                            echo _t("Premium");
                        }
                        ?></span>
                </span>
            <?php } ?>
        </span>

        <span class="item-info">
            <span class="item-name"><?php echo $row['title']; ?></span>
            <span class="item-rate"><?php echo $row['rating']; ?>%</span>
        </span>
    </a>

    <a href="<?php echo $basehttp; ?>/action.php?action=<?php echo ($_GET['mode'] == 'favorites') ? "remove" : "add"; ?>_favorites&id=<?php echo $row['record_num']; ?>" data-mb="modal" data-opt-type="ajax" data-opt-close="<?php echo _t("Close") ?>" data-toggle="tooltip" title="<?php if ($_GET['mode'] == 'favorites') { ?><?php echo _t("Remove from favorites") ?><?php } else { ?><?php echo _t("Add to favorites") ?><?php } ?>" class="add-to-fav">
        <?php if ($_GET['mode'] == 'favorites') { ?>
            <i class="fas fa-minus-circle"></i>
        <?php } else { ?>
            <i class="fas fa-plus-circle"></i>
        <?php } ?>
    </a>
    <?php if ($_SESSION['userid'] && ($row['submitter'] == $_SESSION['userid'])) { ?>
        <a href="<?php echo $basehttp; ?>/edit-content/?id=<?php echo $row['record_num']; ?>" title="<?php echo _t("Edit") ?>" class="edit-content">
            <i class="far fa-edit"></i>
        </a>
    <?php } ?>
</div>
<!-- item END -->