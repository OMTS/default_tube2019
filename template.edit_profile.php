<div class="content-col col">
    <form action="" method="post" enctype="multipart/form-data" name="form1" id="edit-profile-form" class="form-block">
        <div class="user-block">
            <div class="user-aside">
                <div class="user-avatar">
                    <div class="image">
                        <?php if ($urow['avatar'] != '' && file_exists("$basepath/media/misc/{$urow['avatar']}")) { ?>
                            <img src='<?php echo $basehttp; ?>/media/misc/<?php echo $urow['avatar']; ?>' alt= '<?php echo ucwords($urow['username']); ?>'>
                        <?php } else { ?>
                            <?php if (strtolower($urow['gender']) == 'male') { ?>
                                <img src='<?php echo $basehttp; ?>/core/images/avatar_male.png'  alt= '<?php echo ucwords($urow['username']); ?>'>
                            <?php } elseif (strtolower($urow['gender']) == 'female') { ?>
                                <img src='<?php echo $basehttp; ?>/core/images/avatar_female.png'  alt= '<?php echo ucwords($urow['username']); ?>'>
                            <?php } else { ?>
                                <img src='<?php echo $basehttp; ?>/core/images/avatar_default.png'  alt= '<?php echo ucwords($urow['username']); ?>'>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>

                <div class="user-actions">                
                    <div class="fake-upload">
                        <input type="file" name="file" id="fileField" data-opt="upload-btn">
                        <a href="#" title="" class="btn btn-default btn-full" data-mb="fake-upload" data-opt-target="upload-btn"><span class="sub-label"><?php echo _t("Upload avatar") ?></span> <small>(max. <?php echo $max_avatar_size; ?>kB)</small></a>
                    </div>
                </div>
            </div>

            <div class="user-info">
                <div class="pseudo-form">
                    <div class="row">

                        <div class="form-item col -half">
                            <label><?php echo _t("Change Password") ?></label>
                            <input type="text" name="newpassword" id="textfield1" value="" class="form-control" placeholder="<?php echo _t("Change Password") ?>">
                        </div>


                    </div>

                    <div class="row">

                        <div class="form-item col -half">
                            <label><?php echo _t("Email Address") ?></label>
                            <input type="text" name="email" id="textfield2" value="<?php echo $urow['email']; ?>" class="form-control" placeholder="<?php echo _t("Email Address") ?>">
                        </div>

                        <div class="form-item col -half">
                            <label><?php echo _t("Location") ?></label>
                            <input type="text" name="location" id="textfield3" value="<?php echo $urow['location']; ?>" class="form-control" placeholder="<?php echo _t("Location") ?>">
                        </div>


                    </div>

                    <div class="row">

                        <div class="form-item col -half">
                            <label><?php echo _t("Age") ?></label>
                            <select name="age" id="select" data-style="btn-selectpicker">
                                <?php
                                for ($i = 18; $i < 100; $i++) {
                                    if ($urow['age'] == $i) {
                                        $selected = 'selected';
                                    } else {
                                        $selected = '';
                                    }
                                    echo "<option $selected value='$i'> $i </option>";
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-item col -half">
                            <label><?php echo _t("Gender") ?></label>
                            <select name="gender" id="select2" data-style="btn-selectpicker">
                                <option <?php
                                if ($urow['gender'] == 'Male') {
                                    echo 'selected';
                                }
                                ?> value='Male'><?php echo _t("Male") ?></option>
                                <option <?php
                                if ($urow['gender'] == 'Female') {
                                    echo 'selected';
                                }
                                ?> value='Female'><?php echo _t("Female") ?></option>
                            </select>
                        </div>


                    </div>

                    <?php if (is_array($custom_user_fields)) { ?>
                        <div class="row">

                            <?php foreach ($custom_user_fields as $k => $v) { ?>
                                <div class="form-item col -half">

                                    <label><?php echo $k; ?></label>
                                    <?php if (is_array($v)) { ?>
                                        <select name="custom[<?php echo $k; ?>]" data-style="btn-selectpicker">
                                            <?php foreach ($v as $i) { ?>
                                                <option<?php echo ($custom[$k] == $i) ? ' selected' : ''; ?>><?php echo $i; ?></option>
                                            <?php } ?>
                                        </select>
                                    <?php } else { ?>
                                        <input type="text" placeholder="<?php echo $k; ?>" name="custom[<?php echo $k; ?>]" value='<?php echo htmlentities($custom[$k], ENT_QUOTES, 'UTF-8'); ?>' class="form-control custom-field">
                                    <?php } ?>	

                                </div>
                            <?php } ?>


                        </div>
                    <?php } ?>

                    <div class="row">

                        <div class="form-item col -full">
                            <label><?php echo _t("About yourself...") ?></label>
                            <textarea name="description" id="textarea-about" class="form-control" placeholder="<?php echo _t("About yourself") ?>"><?php echo $urow['description']; ?></textarea>
                        </div>


                    </div>

                    <div class="row">

                        <div class="form-item col -full">
                            <label><?php echo _t("E-mail Notifications"); ?></label>

                            <div class="checkbox">
                                <label>
                                    <input id="form-email-new-message" class="checkbox-inline" type="checkbox" value="1" name="notifications[new_message]" <?php echo $urow['notifications']['new_message'] == 1 ? 'checked="checked"' : ''; ?>>
                                    <span class="sub-label"><?php echo _t("Email me when someone sends me a private message"); ?></span>
                                </label>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input id="form-email-new-comment" class="checkbox-inline" type="checkbox" value="1" name="notifications[new_comment]" <?php echo $urow['notifications']['new_comment'] == 1 ? 'checked="checked"' : ''; ?>>
                                    <span class="sub-label"><?php echo _t("Email me when someone posts a comment to my content"); ?></span>
                                </label>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input id="form-email-new-post" class="checkbox-inline" type="checkbox" value="1" name="notifications[new_post]" <?php echo $urow['notifications']['new_post'] == 1 ? 'checked="checked"' : ''; ?>>
                                    <span class="sub-label"><?php echo _t("Email me when someone writes on my wall"); ?></span>
                                </label>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input id="form-email-friend-request" class="checkbox-inline" type="checkbox" value="1" name="notifications[friend_request]" <?php echo $urow['notifications']['friend_request'] == 1 ? 'checked="checked"' : ''; ?>>
                                    <span class="sub-label"><?php echo _t("Email me when someone sends me a friend request"); ?></span>
                                </label>
                            </div>                      
                        </div>


                    </div>

                    <div class="row">
                        
                        <div class="form-item col -actions">
                            <button class="btn btn-default"  type="submit" name="button" id="button"><span class="btn-label"><?php echo _t("Save") ?></span></button>                            
                        </div>
                        
                        
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>