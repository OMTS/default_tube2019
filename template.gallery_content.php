<!-- VIDEO CONTENT -->
<!-- stage -->
<div class="stage-gallery">
    <div class="gallery-block">
        <?php
        echo "<div data-mb='ajax-container' data-opt-close='" . _t("Close") . "' data-opt-title='" . _t("Error") . "' id='singleImage' data-opt-current='" . $_GET['image'] . "'>";
        echo '<div class="loader"><div class="loader-inner ball-triangle-path"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';

        $goToNext = "";
        $goToPrev = "";
        if ($_GET['image'] - 1 > 0) {
            $imagePrevLink = "$galleryUrl?image=" . ($_GET['image'] - 1);
            $goToPrev = $_GET['image'] - 1;
        } else {
            $imagePrevLink = $galleryUrl;
        }
        if ($_GET['image'] + 1 <= count($result)) {
            $imageNextLink = "$galleryUrl?image=" . ($_GET['image'] + 1);
            $goToNext = $_GET['image'] + 1;
        } else {
            $imageNextLink = $galleryUrl;
        }
        ?>
        <a href="<?php echo $imageUrl; ?>" data-lightbox="gallery" title="<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>">
            <img src="<?php echo $imageUrl; ?>" alt="<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?> (<?php echo $_GET['image']; ?>/<?php echo count($result) ?>)">
        </a>
        <?php if (count($result) > 1) { ?>
            <div class="gallery-controls">
                <?php if ($_GET['image'] > 1) { ?>
                    <a rel="prev" data-opt-next="<?php echo $goToPrev; ?>" data-mb="load-img" class="gallery-nav gallery-prev" title="<?php echo _t("Previous") ?> (<?php echo $goToPrev ?>/<?php echo count($result) ?>)" href="<?php echo $imagePrevLink; ?>">
                        <span class="slider-prev"><span></span></span>
                    </a>
                <?php } ?>
                <?php if ($_GET['image'] < count($result)) { ?>
                    <a rel="next" data-opt-next="<?php echo $goToNext; ?>" data-mb="load-img" class="gallery-nav gallery-next" title="<?php echo _t("Next") ?> (<?php echo $goToNext ?>/<?php echo count($result) ?>)" href="<?php echo $imageNextLink; ?>">
                        <span class="slider-next"><span></span></span>
                    </a>
                <?php } ?>
            </div>
            <?php
        }
        echo "</div>";

        echo "<div id='galleryImages' class='row'>";
        echo '<div class="gallery-slider owl-carousel owl-theme owl-loaded" data-mb="slider" data-opt-current-img="' . $_GET['image'] . '" data-opt-img-max="' . count($result) . '">';
        foreach ($result as $k => $row) {
            $j++;
            ?>
            <div class='gallery-item-col col gi-<?php echo $j; ?> <?php echo ($j == $_GET['image']) ? "active" : ""; ?>' >
                <a <?php if (is_numeric($_GET['image'])) { ?>data-mb="load-img" data-opt-next="<?php echo $k + 1; ?>"<?php } ?> title="Image <?php echo $k + 1; ?>/<?php echo count($result) ?>" href="<?php echo $galleryUrl; ?>?image=<?php echo $k + 1; ?>">
                    <img src="<?php echo $gallery_url; ?>/<?php echo $rrow['filename']; ?>/thumbs/<?php echo $row['filename']; ?>" alt="<?php echo $row['title']; ?>" >
                </a>
            </div>
            <?php
        }
        echo '</div>';

        if (count($result) == 0) {
            echo "Sorry, this gallery contains no photos.";
        }
        echo "</div>";
        ?>
    </div>
</div>

<div class="content-info">
    <div class="content-controls">
        <?php include($basepath . '/includes/rating/templateTh_2019.php'); ?>

        <div class="content-tabs">
            <ul class="nav tabs-nav" role="tablist">
                <li>
                    <a class="active" id="about-tab" data-toggle="tab" href="#about" role="tab" aria-controls="about" aria-selected="true"><?php echo _t("About"); ?></a>
                </li>
                <li>
                    <a id="share-tab" data-toggle="tab" href="#share" role="tab" aria-controls="share" aria-selected="false"><?php echo _t("Share"); ?></a>
                </li>
                <?php if ($allowAddFavorites) { ?>
                    <li>
                        <a href="<?php echo $basehttp; ?>/action.php?action=add_favorites&id=<?php echo $rrow['record_num']; ?>" data-mb="modal" data-opt-type="ajax" data-opt-close="<?php echo _t("Close"); ?>" title="<?php echo _t("Add to favorites"); ?>">
                            <i class="icon fas fa-heart"></i>
                            <span class="sub-label"><?php echo _t("Add to favorites"); ?></span>
                        </a>
                    </li>
                <?php } ?>
                <li>
                    <a href="<?php echo $basehttp; ?>/action.php?action=reportVideo&id=<?php echo $rrow['record_num']; ?>" data-mb="modal" data-opt-type="iframe" data-opt-iframe-width="100%"  data-opt-iframe-height="466px" data-toggle="tooltip" title="<?php echo _t("Report"); ?>">
                        <i class="icon fas fa-flag"></i>
                        <span class="sub-label"><?php echo _t("Report"); ?></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="tabs-container">
        <div class="tab-content show active" id="about" role="tabpanel" aria-labelledby="about-tab">
            <?php
            $_description = $rrow['description'];
            $_pornstar = showPornstar($rrow['record_num'], "", "", FALSE);
            $_keywords = buildTags3($rrow['keywords']);
            $_channels = buildChannels3($rrow['record_num']);
            ?>
            <div class="content-base-info">
                <div class="content-submitter">
                    <?php if (!$rrow['submitter'] == 0) { ?>
                        <a href="<?php echo generateUrl('user', $rrow['username'], $rrow['submitter']); ?>" title="<?php echo $rrow['username']; ?>">
                        <?php } ?>
                        <span class="avatar">
                            <?php echo getUserAvatar($rrow['submitter']); ?>
                        </span>
                        <?php if (!$rrow['submitter'] == 0) { ?>
                        </a>
                    <?php } ?>
                    <span class="sub-label">Submitted by</span>
                    <?php if ($rrow['submitter'] == 0) { ?>
                        <?php echo _t("Anonymous") ?>
                    <?php } else { ?>
                        <a href="<?php echo generateUrl('user', $rrow['username'], $rrow['submitter']); ?>" title="<?php echo $rrow['username']; ?>">
                            <?php echo $rrow['username']; ?>
                        </a>
                    <?php } ?>
                </div>

                <div class="info-elem -length">
                    <i class="fas fa-image"></i>
                    <span class="sub-label"><?php echo count($result); ?></span>
                </div>

                <div class="info-elem -length">
                    <i class="far fa-eye"></i>
                    <span class="sub-label"><?php echo $rrow['views']; ?></span>
                </div>

                <div class="info-elem -length">
                    <i class="far fa-calendar"></i>
                    <span class="sub-label"><?php echo $rrow['date_added']; ?></span>
                </div>
            </div>

            <?php if (!empty($_description)) { ?>
                <div class="content-desc">
                    <?php echo $_description; ?>
                </div>
            <?php } ?>
            <?php if ($_pornstar != false) { ?>
                <div class="content-links -models">
                    <div class="label">
                        <i class="far fa-star"></i> <?php echo _t("Modles"); ?>:
                    </div>

                    <?php echo $_pornstar; ?>
                </div>
            <?php } ?>
            <?php if (!empty($_keywords)) { ?>
                <div class="content-links -tags">
                    <div class="label">
                        <i class="fas fa-tag"></i> <?php echo _t("Tags"); ?>:
                    </div>

                    <?php echo $_keywords; ?>
                </div>
            <?php } ?>
            <?php if (!empty($_channels)) { ?>
                <div class="content-links -niches">
                    <div class="label">
                        <i class="fas fa-list-ul"></i> <?php echo _t("Categories"); ?>:
                    </div>

                    <?php echo $_channels; ?>
                </div>
            <?php } ?>
        </div>

        <div class="tab-content" id="share" role="tabpanel" aria-labelledby="share-tab">
            <div class="content-share">
                <div class="content-socials">
                    <ul class="social-list">
                        <li><a data-mb="popup" data-opt-width="500" data-opt-height="400" href="//www.facebook.com/share.php?u=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>&title=<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a data-mb="popup" data-opt-width="500" data-opt-height="400" href="//twitter.com/home?status=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>+<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#" title=""><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#" title=""><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($allowDisplayComments) { ?>
    <div class="content-comments">
        <div class="title-col -comments">
            <i class="far fa-comment-alt"></i> <h4><?php echo _t("Comments"); ?></h4>
        </div>

        <?php $contentID = $rrow['record_num']; ?>
        <?php $commentsType = 0; ?>
        <?php include('widgets/widget.comments.php'); ?>
    </div>
<?php } ?>