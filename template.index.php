<?php

if (is_array($results)) {
    $i = 0;
    foreach ($results as $row) {
        if ($row['langTitle']) {
            $row['title'] = $row['langTitle'];
        }
        if ($row['langDescription']) {
            $row['description'] = $row['langDescription'];
        }
        if ($row['langKeywords']) {
            $row['keywords'] = $row['langKeywords'];
        }
        if ($results_per_row > 0) {
            $i++;
            $rest = $i % $results_per_row;
            if ($rest == 0) {
                $class = ' last';
            } else {
                $class = '';
            }
        }
        if ($_GET['mode'] == 'search' && $_GET['type'] == 'members') {
            getTemplate("template.member_item.php");
        } else {
            if ($row['photos'] == 1) {
                getTemplate("template.content_item_photo.php");
            } else {
                getTemplate("template.content_item.php");
            }
        }
    }
} else {
    echo '<div class="notification-col col">';
    echo setMessage(_t("Sorry, no results were found."), 'alert', true);
    echo "</div>";
}
?>