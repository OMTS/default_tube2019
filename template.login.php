<div class="form-col col">
    <form class="form-block" id="login-form"  name="loginForm" method="post" action="">
        <div class="row">

            <div class="form-item col -half">
                <input class="form-control" id="ahd_username" name="ahd_username" autocomplete='off' type="text" value="" placeholder="<?php echo _t("Username") ?>">
            </div>

            <div class="form-item col -half">
                <input class="form-control" id="ahd_password" name="ahd_password" autocomplete='off' type="password" value="" placeholder="<?php echo _t("Password") ?>">
            </div>


        </div>

        <div class="row">

            <div class="form-item col -links">
                <a href="<?php echo $basehttp; ?>/forgot-pass"><?php echo _t("Forgot Password?") ?></a><br>
                <a href="<?php echo $basehttp; ?>/signup"><?php echo _t("Not a member? Click here to sign up, its free!") ?></a>
            </div>


        </div>

        <div class="row">

            <div class="form-item col -actions">
                <button class="btn btn-default" type="submit" name="Submit"><span class="btn-label"><?php echo _t("Login") ?></span></button>
            </div>


        </div>

        <?php if (!$_SESSION['userid'] && ($enable_facebook_login || $enable_twitter_login)) { ?>
            <div class="row social-logins">

                <?php if (!$_SESSION['userid'] && $enable_facebook_login) { ?>
                    <div class="form-item col -social -social-fb">
                        <?php include($basepath . '/facebook_login.php'); ?>					
                        <a class="btn btn-social-login btn-social-fb" href="<?php echo $basehttp; ?>/includes/facebook/facebook.php"><img src="<?php echo $basehttp; ?>/core/images/facebook-login-button.png" alt="<?php echo _t("Login by FaceBook") ?>" border="0" /></a>
                    </div>
                <?php } ?>

                <?php if (!$_SESSION['userid'] && $enable_twitter_login) { ?>
                    <div class="form-item col -social -social-tw">
                        <a href="<?php echo $twitter->getAuthenticateUrl(); ?>&force_login=true"><img src="<?php echo $basehttp; ?>/core/images/twitter-login-button.png" alt="<?php echo _t("Login by Twitter") ?>" /></a>
                    </div>
                <?php } ?>


            </div>
        <?php } ?>
    </form>
</div>