<?php
$link = generateUrl('user', $row['username'], $row['record_num']);
?>
<!-- item -->
<div class="item-col col -member">
    <a href="<?php echo $link; ?>" title="<?php echo ucwords($row['username']); ?>">
        <span class="image">
            <?php if ($row['avatar'] != '' && file_exists("$basepath/media/misc/{$row['avatar']}")) { ?>
                <img src='<?php echo $basehttp; ?>/media/misc/<?php echo $row['avatar']; ?>' alt= '<?php echo ucwords($row['username']); ?>'>
            <?php } else { ?>
                <?php if (strtolower($row['gender']) == 'male') { ?>
                    <img src='<?php echo $basehttp; ?>/core/images/avatar_male.png'  alt= '<?php echo ucwords($row['username']); ?>'>
                <?php } elseif (strtolower($row['gender']) == 'female') { ?>
                    <img src='<?php echo $basehttp; ?>/core/images/avatar_female.png'  alt= '<?php echo ucwords($row['username']); ?>'>
                <?php } else { ?>
                    <img src='<?php echo $basehttp; ?>/core/images/avatar_default.png'  alt= '<?php echo ucwords($row['username']); ?>'>
                <?php } ?>
            <?php } ?>
        </span>

        <span class="item-info">
            <span class="item-name"><?php echo ucwords($row['username']); ?></span>
            <?php if ($row['gender'] !== '') { ?>
                <span class="item-rate">
                    <?php if (strtolower($row['gender']) == 'male') { ?>
                        <i class="fas fa-mars"></i>
                    <?php } else { ?>
                        <i class="fas fa-venus"></i>
                    <?php } ?>
                </span>
            <?php } ?>
        </span>
    </a>
</div>
<!-- item END -->