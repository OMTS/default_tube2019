<?php
$resultNew = dbQuery("SELECT  friends.record_num as friend_id, friends.friend_request_message, friends.date_added,  users.record_num, users.username FROM friends, users 
		          WHERE friends.friend = '$_SESSION[userid]' AND users.record_num =  friends.user AND approved = 0 ORDER BY friends.date_added", false);
?>
<div class="content-col col">
    <div class="user-block">
        <div class="user-aside">
            <div class="user-avatar">
                <div class="image">
                    <?php if ($urow['avatar'] != '' && file_exists("$basepath/media/misc/{$urow['avatar']}")) { ?>
                        <img src='<?php echo $basehttp; ?>/media/misc/<?php echo $urow['avatar']; ?>' alt= '<?php echo ucwords($urow['username']); ?>'>
                    <?php } else { ?>
                        <?php if (strtolower($urow['gender']) == 'male') { ?>
                            <img src='<?php echo $basehttp; ?>/core/images/avatar_male.png'  alt= '<?php echo ucwords($urow['username']); ?>'>
                        <?php } elseif (strtolower($urow['gender']) == 'female') { ?>
                            <img src='<?php echo $basehttp; ?>/core/images/avatar_female.png'  alt= '<?php echo ucwords($urow['username']); ?>'>
                        <?php } else { ?>
                            <img src='<?php echo $basehttp; ?>/core/images/avatar_default.png'  alt= '<?php echo ucwords($urow['username']); ?>'>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

            <div class="user-actions">                
                <a href="<?php echo $basehttp; ?>/edit-profile" title="<?php echo _t("Edit profile"); ?>" class="btn btn-dark"><?php echo _t("Edit profile"); ?></a>
                <?php $countInbox = dbValue("SELECT COUNT(`record_num`) AS `count` FROM `mail` WHERE `to_user` = '{$_SESSION['userid']}' AND `recipient_deleted` = 0 AND `recipient_read` = 0", 'count'); ?>
                <a href="<?php echo $basehttp; ?>/mailbox/" title="<?php echo _t("Messages") ?>" class="btn btn-default"><?php echo _t("Messages") ?> - <?php echo $countInbox; ?></a>
            </div>
        </div>

        <div class="user-info">
            <div class="tab-content active show" id="new" role="tabpanel" aria-labelledby="new-tab">
                <header class="row">

                    <div class="title-col col">
                        <h2><?php echo _t("New invitations"); ?></h2>
                    </div>


                </header>

                <div class="row">

                    <div class="invitations-col col -full">
                        <form method="post" action="">
                            <table width="100%" border="0" class="sTable">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="150"><div align="left"><?php echo _t("User") ?></div></th>
                                        <th class="text-center"><div align="left"><?php echo _t("Message") ?></div></th>
                                        <th class="text-center"><div class="text-center"><?php echo _t("Approve") ?></div></th>	
                                        <th class="text-center"  width="90"><div align="left"><?php echo _t("Sent") ?></div></th>
                                        <th class="text-center"  width="60"><div class="text-center"><?php echo _t("Delete") ?></div></th>
                                    </tr>	
                                </thead>
                                <tbody>
                                    <?php
                                    if ($resultNew) {
                                        foreach ($resultNew as $nrow) {
                                            $link = generateUrl('user', $nrow['username'], $nrow['record_num']);
                                            ?>
                                            <tr id="friend<?php echo $nrow['friend_id']; ?>">
                                                <td><a href="<?php echo $link; ?>"><?php echo $nrow['username']; ?></a></td>
                                                <td><?php echo $nrow['friend_request_message']; ?></td>
                                                <td>
                                                    <div class="text-center">
                                                        <a onClick="javascript:approveFriend('<?php echo $nrow['friend_id']; ?>'); return false;" href="#" class="addIco">
                                                            <i class="fas fa-check-circle"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td><strong><?php echo datediff('', $nrow['date_added'], date('Y-m-d H:i:s'), false); ?></strong> <?php echo _t("ago"); ?></td>        
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <a onClick="javascript:deleteFriend('<?php echo $nrow['friend_id']; ?>'); return false;" href="#" class="removeIco">
                                                            <i class="fas fa-trash"></i>
                                                        </a>
                                                    </div>
                                                </td>		
                                            </tr>
                                        <?php } ?>				
                                    <td colspan="5" class="no-border">
                                        <a class="btn btn-default" href="<?php echo $basehttp; ?>/my-friends?acceptAll=1"><?php echo _t("Accept ALL") ?></a>
                                        <a href="<?php echo $basehttp; ?>/my-friends?declineAll=1" class="btn btn-dark"><?php echo _t("Decline  ALL") ?></a>
                                    </td>	
                                <?php } else {
                                    ?>
                                    <td colspan="5" class="no-border"><?php echo _t("No new invation") ?></td>
                                <?php } ?>
                                </tbody>
                            </table>
                        </form>
                    </div>


                </div>
            </div>

            <div class="tab-content" id="friends" role="tabpanel" aria-labelledby="friends-tab">
                <header class="row">

                    <div class="title-col col">
                        <h2><?php echo _t("My Friends"); ?></h2>
                    </div>


                </header>

                <div class="row">

                    <div class="invitations-col col -full">
                        <form method="post" action="">
                            <table width="100%" border="0" class="sTable">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="150"><div align="left"><a href="?sortby=name"><?php echo _t("User") ?></a></div></th>
                                        <th class="text-center"><div align="left"><a href="?sortby=date"><?php echo _t("Date added") ?></a></div></th>			
                                        <th class="text-center" width="60"><div class="text-center"><?php echo _t("Delete") ?></div></th>
                                    </tr>	
                                </thead>
                                <tbody>
                                    <?php
                                    if (isset($_GET['sortby']) && $_GET['sortby'] == 'name') {
                                        $orderBy = ' `users`.`username` ASC ';
                                    } else if (isset($_GET['sortby']) && $_GET['sortby'] == 'date') {
                                        $orderBy = ' `friends`.`date_added` DESC ';
                                    } else {
                                        $orderBy = ' `friends`.`date_added` DESC ';
                                    }
                                    $resultFriends = dbQuery("SELECT `friends`.`record_num` as friend_id, `friends`.`friend_request_message`, `friends`.`date_added`,  `users`.`record_num`, `users`.`username` 
                                                              FROM `friends`, `users` 
                                                              WHERE `friends`.`friend` = '{$_SESSION['userid']}' AND `users`.`record_num` = `friends`.`user` AND `approved` = 1 
                                                              ORDER BY $orderBy", false);

                                    if ($resultFriends) {
                                        foreach ($resultFriends as $frow) {
                                            $link = generateUrl('user', $frow['username'], $frow['record_num']);
                                            ?>
                                            <tr id="friend<?php echo $frow['friend_id']; ?>">
                                                <td><a href="<?php echo $link; ?>"><?php echo $frow['username']; ?></a></td>			
                                                <td><strong><?php echo datediff('', $frow['date_added'], date('Y-m-d H:i:s'), false); ?></strong> <?php echo _t("ago") ?></td>
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <a onClick="javascript:deleteFriendApproved('<?php echo $frow[friend_id]; ?>'); return false;" class="removeIco" href="#">
                                                            <i class="fas fa-trash"></i>
                                                        </a>
                                                    </div>
                                                </td>		
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                    <td colspan="3"><?php echo _t("You have no friends") ?>.</td>
                                <?php } ?>
                                </tbody>
                            </table>
                        </form>
                    </div>


                </div>
            </div>

            <div class="tab-content" id="sent" role="tabpanel" aria-labelledby="sent-tab">
                <header class="row">

                    <div class="title-col col">
                        <h2><?php echo _t("Sent Invitations"); ?></h2>
                    </div>


                </header>

                <div class="row">

                    <div class="invitations-col col -full">
                        <form method="post" action="">
                            <table width="100%" border="0" class="sTable">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="150"><div align="left"><?php echo _t("User") ?></div></th>	
                                        <th class="text-center"><div align="left"><?php echo _t("Message") ?></div></th>		
                                        <th class="text-center" width="90"><div align="left"><?php echo _t("Sent") ?></div></th>			
                                        <th class="text-center" width="60"><div class="text-center"><?php echo _t("Delete") ?></div></th>
                                    </tr>	
                                </thead>
                                <tbody>
                                    <?php
                                    $resultSent = dbQuery("SELECT `friends`.`record_num` as friend_id, `friends`.`friend_request_message`, `friends`.`date_added`, `users`.`record_num`, `users`.`username` 
                                                           FROM `friends`, `users` 
                                                           WHERE  `friends`.`user` = '{$_SESSION['userid']}' AND `users`.`record_num` = `friends`.`friend` AND `friends`.`approved` = 0 
                                                           ORDER BY `friends`.`date_added` DESC", false);
                                    if ($resultSent) {
                                        foreach ($resultSent as $srow) {
                                            $link = generateUrl('user', $srow['username'], $srow['record_num']);
                                            ?>
                                            <tr id="friend<?php echo $srow['friend_id']; ?>">
                                                <td><a href="<?php echo $link; ?>"><?php echo $srow['username']; ?></a></td>
                                                <td><?php echo $srow['friend_request_message']; ?></td>
                                                <td class="text-center">
                                                    <div align="left">
                                                        <strong><?php echo datediff('', $srow['date_added'], date('Y-m-d H:i:s'), false); ?></strong> <?php echo _t("ago"); ?>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <div class="text-center">
                                                        <a onclick="javascript:deleteFriend('<?php echo $srow[friend_id]; ?>'); return false;" href="#" class="removeIco">
                                                            <i class="fas fa-trash"></i>
                                                        </a>
                                                    </div>
                                                </td>		
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr><td colspan="4"><?php echo _t("No result") ?></td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<?php getWidget('widget.ad_content_side.php'); ?>