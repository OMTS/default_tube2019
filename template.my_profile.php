<div class="content-col col">
    <div class="user-block">
        <div class="user-aside">
            <div class="user-avatar">
                <div class="image">
                    <?php if ($urow['avatar'] != '' && file_exists("$basepath/media/misc/{$urow['avatar']}")) { ?>
                        <img src='<?php echo $basehttp; ?>/media/misc/<?php echo $urow['avatar']; ?>' alt= '<?php echo ucwords($urow['username']); ?>'>
                    <?php } else { ?>
                        <?php if (strtolower($urow['gender']) == 'male') { ?>
                            <img src='<?php echo $basehttp; ?>/core/images/avatar_male.png'  alt= '<?php echo ucwords($urow['username']); ?>'>
                        <?php } elseif (strtolower($urow['gender']) == 'female') { ?>
                            <img src='<?php echo $basehttp; ?>/core/images/avatar_female.png'  alt= '<?php echo ucwords($urow['username']); ?>'>
                        <?php } else { ?>
                            <img src='<?php echo $basehttp; ?>/core/images/avatar_default.png'  alt= '<?php echo ucwords($urow['username']); ?>'>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

            <div class="user-actions">                
                <a href="<?php echo $basehttp; ?>/edit-profile" title="<?php echo _t("Edit profile"); ?>" class="btn btn-dark"><?php echo _t("Edit profile"); ?></a>
                <?php $countInbox = dbValue("SELECT COUNT(`record_num`) AS `count` FROM `mail` WHERE `to_user` = '{$_SESSION['userid']}' AND `recipient_deleted` = 0 AND `recipient_read` = 0", 'count'); ?>
                <a href="<?php echo $basehttp; ?>/mailbox/" title="<?php echo _t("Messages") ?>" class="btn btn-default"><?php echo _t("Messages") ?> - <?php echo $countInbox; ?></a>
            </div>
        </div>

        <div class="user-info">
            <div class="title-col -sub">
                <h2><?php echo _t("User Informations"); ?></h2>
            </div>

            <ul class="user-list">
                <li><span class="sub-label"><?php echo _t("Username") ?>:</span> <span class="desc"><?php echo $urow['username']; ?></span></li>
                <li><span class="sub-label"><?php echo _t("Joined") ?>:</span> <span class="desc"><?php echo date('F jS Y', strtotime($urow['date_joined'])); ?></span></li>
                <li><span class="sub-label"><?php echo _t("Last Login") ?>:</span> <span class="desc"><?php echo $urow['lastlogin'] ? date('Y-m-d \a\t H:i:s', $urow['lastlogin']) : 'Never'; ?></span></li>
                <?php if ($urow['gender']) { ?>
                    <li><span class="sub-label"><?php echo _t("Gender") ?>:</span> <span class="desc"><?php echo _t($urow['gender']); ?></span></li>
                <?php } ?>
                <?php if ($urow['age']) { ?>
                    <li><span class="sub-label"><?php echo _t("Age") ?>:</span> <span class="desc"><?php echo $urow['age']; ?></span></li>
                <?php } ?>
                <?php if ($urow['location']) { ?>
                    <li><span class="sub-label"><?php echo _t("Location") ?>:</span> <span class="desc"><?php echo $urow['location']; ?></span></li>
                <?php } ?>
                <?php foreach ($custom_user_fields as $k => $v) { ?>
                    <?php if ($custom[$k]) { ?>
                        <li><span class="sub-label"><?php echo $k; ?>:</span> <span class="desc"><?php echo $custom[$k]; ?></span></li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>

        <?php if ($urow['description']) { ?>
            <div class="user-dec">
                <div class="title-col -sub">
                    <h2><?php echo _t("A little about me") ?>:</h2>
                </div>

                <p><?php echo nl2br($urow['description']); ?></p>
            </div>
        <?php } ?>
    </div>
</div>

<?php getWidget('widget.ad_content_side.php'); ?>

</div>

<div class="row">

    <div class="user-tabs col">
        <div class="tab-content active show" id="videos" role="tabpanel" aria-labelledby="videos-tab">
            <header class="row">

                <div class="title-col col">
                    <h2><?php echo _t("My"); ?> <?php echo _t("Recent Video Uploads") ?></h2>
                    <a href="<?php echo $basehttp; ?>/uploads-by-user/<?php echo $_SESSION['userid']; ?>/" title="<?php echo _t("see more"); ?>"><?php echo _t("see more"); ?></a>
                </div>


            </header>

            <div class="row">

                <?php userRecentUploads($_SESSION['userid'], 10); ?>


            </div>
        </div>

        <div class="tab-content" id="photos" role="tabpanel" aria-labelledby="photos-tab">
            <header class="row">

                <div class="title-col col">
                    <h2><?php echo _t("My"); ?> <?php echo _t("Recent Photo Uploads") ?></h2>
                    <a href="<?php echo $basehttp; ?>/uploads-by-user/<?php echo $_SESSION['userid']; ?>/?photos=1" title="<?php echo _t("see more"); ?>"><?php echo _t("see more"); ?></a>
                </div>


            </header>

            <div class="row">

                <?php userRecentUploads($_SESSION['userid'], 10, 'photos'); ?>


            </div>
        </div>

        <div class="tab-content" id="wall" role="tabpanel" aria-labelledby="wall-tab">
            <header class="row">

                <div class="title-col col">
                    <h2><?php echo _t("My"); ?> <?php echo _t("Wall") ?></h2>
                </div>


            </header>

            <div class="row">

                <?php
                $contentID = $urow['record_num'];
                $commentsType = 2;
                include('widgets/widget.comments.php');
                ?>


            </div>
        </div>

        <div class="tab-content" id="friends" role="tabpanel" aria-labelledby="friends-tab">
            <header class="row">

                <div class="title-col col">
                    <h2><?php echo _t("My"); ?> <?php echo _t("Friends") ?></h2>
                    <a href="<?php echo $basehttp; ?>/my-friends" title="<?php echo _t("see more"); ?>"><?php echo _t("see more"); ?></a>
                </div>


            </header>

            <div class="row">

                <?php getUsersFriends($_SESSION['userid'], 8); ?>


            </div>
        </div>
    </div>