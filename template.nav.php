<header class="header-sec">
    <div class="top-bar">
        <div class="wrapper">
            <div class="row">

                <div class="ucp-col col">
                    <?php if (!isset($_SESSION['userid'])) { ?>
                        <ul class="ucp-list">
                            <li><a href="<?php echo $basehttp; ?>/login" title="<?php echo _t("Login") ?>"><?php echo _t("Login") ?></a></li>
                            <li>/</li>
                            <li><a href="<?php echo $basehttp; ?>/signup" title="<?php echo _t("Sign up") ?>"><?php echo _t("Sign up") ?></a></li>
                        </ul>
                    <?php } else { ?>
                        <button id="sUcp" class="btn btn-dropdown btn-ucp" aria-expanded="false" aria-haspopup="true" data-toggle="dropdown">
                            <span class="avatar">
                                <span class="avatar-img">
                                    <?php echo getUserAvatar($_SESSION['userid']); ?>
                                </span>
                            </span>
                            <span class="user-name">
                                <?php echo $_SESSION['username']; ?>
                            </span>
                            <i class="fas fa-angle-down"></i>
                        </button>

                        <ul class="ucp-dropdown dropdown-menu dropdown-menu-right" aria-labelledby="sUcp">
                            <li><a title="<?php echo _t("My profile") ?>" href="<?php echo $basehttp; ?>/my-profile"><?php echo _t("My profile") ?></a></li>
                            <li><a title="<?php echo _t("Edit profile") ?>" href="<?php echo $basehttp; ?>/edit-profile"><?php echo _t("Edit profile") ?></a></li>
                            <li>
                                <?php $countInbox = dbValue("SELECT COUNT(`record_num`) AS `count` FROM `mail` WHERE `to_user` = '{$_SESSION['userid']}' AND `recipient_deleted` = 0 AND `recipient_read` = 0", 'count'); ?>
                                <a title="<?php echo _t("Messages") ?>" href="<?php echo $basehttp; ?>/mailbox/"><?php echo _t("Messages") ?> (<?php echo $countInbox; ?>)</a>
                            </li>
                            <?php if ($allowAddFavorites) { ?>
                                <li><a title="<?php echo _t("Favorites") ?>" href="<?php echo $basehttp; ?>/favorites/"><?php echo _t("Favorites") ?></a></li>
                            <?php } ?>
                            <li>
                                <?php $countFriendsInvites = dbValue("SELECT COUNT(`record_num`) AS `count` FROM `friends` WHERE `friend` = '{$_SESSION['userid']}' AND `approved` = 0", 'count'); ?>
                                <a title="<?php echo _t("Friends") ?>" href="<?php echo $basehttp; ?>/my-friends"><?php echo _t("Friends") ?> (<?php echo $countFriendsInvites; ?>)</a>
                            </li>
                            <?php if ($vodMode) { ?>
                                <li>
                                    <?php $userNumTokens = billingGetUserTokens($_SESSION['userid']); ?>
                                    <a title="<?php echo _t("Buy Tokens") ?>" href="<?php echo $basehttp; ?>/buy-tokens"><?php echo _t("Buy Tokens") ?> (<?php echo (int) $userNumTokens['tokens']; ?>)</a>
                                </li>
                            <?php } ?>
                            <li><a title="<?php echo _t("Logout") ?>" href="<?php echo $basehttp; ?>/logout"><?php echo _t("Logout") ?></a></li>
                        </ul>
                    <?php } ?>
                </div>

                <?php $langsNav = flags(); ?>
                <div class="lang-col col">
                    <?php if (is_array($langsNav['list']) && count($langsNav['list']) > 0) { ?>
                        <button id="sLang" class="btn btn-dropdown" aria-expanded="false" aria-haspopup="true" data-toggle="dropdown">
                            <span class="btn-label"><?php echo $langsNav['current']['iso']; ?></span>
                            <i class="fas fa-angle-down"></i>
                        </button>

                        <ul class="lang-dropdown dropdown-menu dropdown-menu-right" aria-labelledby="sLang">
                            <?php foreach ($langsNav['list'] as $item) { ?>
                                <li>
                                    <a href="<?php echo $item['http']; ?>" title=""><?php echo $item['iso']; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <div id="sLang" class="btn btn-dropdown">
                            <span class="btn-label"><?php echo $langsNav['current']['iso']; ?></span>
                        </div>
                    <?php } ?>
                </div>


            </div>
        </div>
    </div>

    <div class="main-header">
        <div class="wrapper">
            <div class="row">

                <div class="logo-col col">
                    <h1>
                        <a href="<?php echo $basehttp; ?>" title="<?php echo $sitename; ?>">
                            <img src="<?php echo $template_url; ?>/images/logo.png" alt="<?php echo _t("Home") ?> - <?php echo $sitename; ?>">
                        </a>
                    </h1>
                </div>

                <div class="nav-col col">
                    <div class="nav-inner-col inner-col" data-container="nav">
                        <ul class="main-nav">
                            <li class="nav-elem has-list<?php if (checkNav('videos')) echo ' active'; ?>">
                                <a href="<?php echo $basehttp; ?>/videos/" title="<?php echo _t("Movies") ?>">
                                    <span class="sub-label"><?php echo _t("Movies"); ?></span>
                                    <i class="fas fa-angle-down"></i>
                                </a>

                                <a href="#" class="show-drop">
                                    <i class="fas fa-angle-down"></i>
                                </a>

                                <ul class="nav-drop">
                                    <li><a href="<?php echo $basehttp; ?>/videos/" title="<?php echo _t("Most Recent") ?>"><?php echo _t("Most Recent") ?></a></li>
                                    <li><a href="<?php echo $basehttp; ?>/most-viewed/" title="<?php echo _t("Most Viewed") ?>"><?php echo _t("Most Viewed") ?></a></li>
                                    <li><a href="<?php echo $basehttp; ?>/top-rated/" title="<?php echo _t("Top Rated") ?>"><?php echo _t("Top Rated") ?></a></li>
                                    <li><a href="<?php echo $basehttp; ?>/most-discussed/" title="<?php echo _t("Most Discussed") ?>"><?php echo _t("Most Discussed") ?></a></li>
                                    <li><a href="<?php echo $basehttp; ?>/longest/" title="<?php echo _t("Longest") ?>"><?php echo _t("Longest") ?></a></li>
                                    <li><a href="<?php echo $basehttp; ?>/vr/" title="<?php echo _t("VR"); ?>"><?php echo _t("VR"); ?></a></li>
                                </ul>
                            </li>

                            <li class="nav-elem has-list<?php if (checkNav('photos')) echo ' active'; ?>">
                                <a href="<?php echo $basehttp; ?>/photos/" title="<?php echo _t("Albums"); ?>">
                                    <span class="sub-label"><?php echo _t("Albums"); ?></span>
                                    <i class="fas fa-angle-down"></i>
                                </a>

                                <a href="#" class="show-drop">
                                    <i class="fas fa-angle-down"></i>
                                </a>

                                <ul class="nav-drop">
                                    <li><a href="<?php echo $basehttp; ?>/photos/" title="<?php echo _t("Most Recent") ?>"><?php echo _t("Most Recent") ?></a></li>
                                    <li><a href="<?php echo $basehttp; ?>/photos/most-viewed/" title="<?php echo _t("Most Viewed") ?>"><?php echo _t("Most Viewed") ?></a></li>
                                    <li><a href="<?php echo $basehttp; ?>/photos/top-rated/" title="<?php echo _t("Top Rated") ?>"><?php echo _t("Top Rated") ?></a></li>
                                    <li><a href="<?php echo $basehttp; ?>/photos/most-discussed/" title="<?php echo _t("Most Discussed") ?>"><?php echo _t("Most Discussed") ?></a></li>
                                </ul>
                            </li>

                            <li class="nav-elem has-drop<?php if (checkNav('channels') || $_GET['mode'] == 'channel') echo ' active'; ?>">
                                <a href="<?php echo $basehttp; ?>/channels/" title="<?php echo _t("Categories"); ?>">
                                    <span class="sub-label"><?php echo _t("Categories"); ?></span>
                                    <i class="fas fa-angle-down"></i>
                                </a>

                                <a href="#" class="show-drop">
                                    <i class="fas fa-angle-down"></i>
                                </a>

                                <div class="nav-channels">
                                    <div class="wrapper">
                                        <div class="row">

                                            <?php showChannelsBlocks(array('type' => 'navigation', 'limit' => 6)); ?>

                                            <div class="item-col col -channel -see-all">
                                                <a href="<?php echo $basehttp; ?>/channels/" title="<?php echo _t("See All"); ?>">
                                                    <span class="image">
                                                        <i class="far fa-plus-square"></i>
                                                    </span>

                                                    <span class="item-info">
                                                        <span class="item-name"><?php echo _t("See All"); ?></span>
                                                    </span>
                                                </a>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="nav-elem<?php if (checkNav('pornstars')) echo ' active'; ?>">
                                <a href="<?php echo $basehttp; ?>/models/" title="<?php echo _t("Models"); ?>">
                                    <span class="sub-label"><?php echo _t("Models"); ?></span>
                                </a>
                            </li>

                            <li class="nav-elem<?php if (checkNav('members')) echo ' active'; ?>">
                                <a href="<?php echo $basehttp; ?>/members/" title="<?php echo _t("Community"); ?>">
                                    <span class="sub-label"><?php echo _t("Community"); ?></span>
                                </a>
                            </li>

                            <li class="nav-elem<?php if (checkNav('paysites')) echo ' active'; ?>">
                                <a href="<?php echo $basehttp; ?>/paysites/" title="<?php echo _t("Paysites"); ?>">
                                    <span class="sub-label"><?php echo _t("Paysites"); ?></span>
                                </a>
                            </li>

                            <li class="nav-elem<?php if (checkNav('tags')) echo ' active'; ?>">
                                <a href="<?php echo $basehttp; ?>/tags" title="<?php echo _t("Tags"); ?>">
                                    <span class="sub-label"><?php echo _t("Tags"); ?></span>
                                </a>
                            </li>

                            <li class="nav-elem -upload<?php if (checkNav('upload')) echo ' active'; ?>">
                                <a href="<?php echo $basehttp; ?><?php echo isset($_SESSION['userid']) ? "/upload" : "/login"; ?>" title="<?php echo _t("Upload"); ?>">
                                    <span class="sub-label"><?php echo _t("Upload"); ?></span>
                                </a>
                            </li>
                            <?php if ($_SESSION['userid'] && $vodMode) { ?>
                                <li class="menu-el<?php if (checkNav('purchased')) echo ' active'; ?>">
                                    <a href="<?php echo $basehttp; ?>/my-rentals/" title="<?php echo _t("My Rentals") ?>">
                                        <span class="sub-label"><?php echo _t("My Rentals") ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="search-box" data-container="search">
                        <form action="<?php echo $basehttp; ?>/searchgate.php" method="GET" >
                            <div class="search-wrap">
                                <input type="text" placeholder="<?php echo _t("Search") ?>..." value="<?php echo str_replace("-", " ", $_GET['q']); ?><?php echo str_replace("-", " ", $_GET['letter']); ?>" name="q" class="">
                                <button class="btn btn-search" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="trigger-col col">
                    <button class="btn btn-trigger btn-trigger-search" data-mb="trigger" data-target="search">
                        <i class="fas fa-search"></i>
                    </button>

                    <button class="btn btn-trigger btn-trigger-nav" data-mb="trigger" data-target="nav">
                        <i class="fas fa-bars"></i>
                    </button>
                </div>


            </div>
        </div>
    </div>
</header>