<section class="notification-sec">
    <div class="wrapper">
        <div class="row">

            <!-- notice -->
            <div class="notice-col col-full col text-center">
                <div class="notice-inner-col inner-col">
                    <?php
                    echo getMessages();

                    if (($_GET['controller'] == 'my_friends' || $_GET['controller'] == 'my_profile') && isLoggedIn()) {
                        $friendsInvitations = dbQuery("SELECT  friends.record_num as friend_id, friends.friend_request_message, friends.date_added,  users.record_num, users.username 
                                                   FROM friends, users 
                                                   WHERE  friends.friend = '$_SESSION[userid]' AND users.record_num =  friends.user AND approved = 0 
                                                   ORDER BY friends.date_added", false);
                        if (count($friendsInvitations)) {
                            ?>
                            <div class="notification info">
                                <a href="<? echo $basehttp; ?>/my-friends"><?php echo _t("You have (%html_strong_open %countFriends %html_strong_close) new friend request(s).", array("%html_strong_open" => "<strong>", "%html_strong_close" => "</strong>", "%countFriends" => count($friendsInvitations))); ?></a>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <?php if ($_GET["controller"] == "contact") { ?>
                        <!-- CONTACT PAGE -->
                        <?php if (!$errors && !$success) { ?>
                            <div class="notification info">
                                <?php echo _t("Please use this form to contact the %sitename administrator with any questions or concerns.", array("%sitename" => $sitename)); ?>
                            </div>	
                        <?php } ?>
                        <!-- CONTACT PAGE END -->
                    <?php } ?>
                    <?php
                    if ($_GET["controller"] == "signup") {
                        if (!$errors && $_GET['done'] !== 'true') {
                            ?>	
                            <div class="notification info">
                                <?php echo _t("Sign up for a personal account to save videos, leave comments and utilize other advanced features!"); ?>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <?php if ($_GET["controller"] == "upload" && $allowThisUpload) { ?>
                        <!-- UPLOAD -->
                        <?php if ($_GET['success']) { ?>
                            <div class="notification success">
                                <?php echo _t("Your %optionTitle has been submitted, and will appear on the site shortly pending moderator approval.", array("%optionTitle" => $optionTitle)); ?>
                            </div>
                        <?php } else { ?>
                            <div class="notification info">
                                <?php echo _t("I certify that all models depicted are at least %html_strong_open 18 years of age %html_strong_close.", array("%html_strong_open" => "<strong>", "%html_strong_close" => "</strong>")); ?><br />
                                <?php echo _t("I certify that %html_strong_open I am the owner %html_strong_close or licensed to use this content.", array("%html_strong_open" => "<strong>", "%html_strong_close" => "</strong>")); ?><br />
                                <?php echo _t("I certify that %html_strong_open I have consent %html_strong_close of all models depicted to submit this content.", array("%html_strong_open" => "<strong>", "%html_strong_close" => "</strong>")); ?><br /><br />
                                <?php echo _t("You are logged in as %html_strong_open %username %html_strong_close and your IP is %html_strong_open %userip %html_strong_close", array("%html_strong_open" => "<strong>", "%html_strong_close" => "</strong>", "%username" => $_SESSION['username'], "%userip" => $_SERVER['REMOTE_ADDR'])); ?>
                            </div>	
                        <?php } ?>
                    <?php } ?>
                    <?php if ($_GET["controller"] == "forgot_pass") { ?>
                        <!-- FORGOT PASS -->
                        <?php if (!$errors && !$success) { ?>
                            <div class="notification info">
                                <p><?php echo _t("Fill out this form to have your password emailed to you") ?>.</p>
                            </div>
                        <?php } ?>
                        <?php if (isset($_GET[done]) && $_GET[done] == 'true') { ?>
                            <div class="notification success">
                                <p>
                                    <?php if ($require_account_confirmation) { ?>
                                        <?php echo _t("Verification mail was sent to you email address") ?>.<br />
                                        <?php echo _t("To login please verify your account through email verification link") ?>.
                                    <?php } else { ?>
                                        <?php echo _t("You may now login at the") ?> <a href="<? echo $basehttp; ?>/login"><?php echo _t("login page") ?></a>
                                    <?php } ?>
                                </p>
                            </div>
                        <?php } ?>	
                    <?php } ?>
                    <?php if ($_GET["controller"] == "login") { ?>
                        <!-- LOGIN FORM -->
                        <?php if ($errors) { ?>

                        <?php } else { ?>
                            <div class="notification info">
                                <p>
                                    <?php echo _t("You may login to your account using the form below") ?>.<br>
                                    <a href='<? echo $basehttp; ?>/signup'><?php echo _t("Not a member? Click here to sign up, its free!") ?></a>
                                </p>
                            </div>	
                        <?php } ?>
                    <?php } ?>
                    <?php if ($_GET["controller"] == "user_profile") { ?>
                        <!-- USER PROFILE -->
                        <?php if ($success) echo '<div class="notification success">' . $success . '</div>'; ?>
                    <?php } ?>
                </div>
            </div>
            <!-- notice END -->


        </div>
    </div>
</section>