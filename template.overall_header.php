<?php $colorClass = ($_COOKIE["mbColorScheme"] == 1) ? " inverted-colors" : ""; ?><!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7<?php echo $colorClass ?>"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie-7-only<?php echo $colorClass ?>"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie-8-only<?php echo $colorClass ?>"><![endif]-->
<!--[if gte IE 9]><!--> <html class="no-js no-filter<?php echo $colorClass ?>"><!--<![endif]-->
    <head>
        <?php getWidget('widget.header_scripts.php'); ?>
    </head>
    <body class="page-<?php echo bodyClasses(); ?>">

        <section class="page-wrap">
            <?php getTemplate('template.nav.php'); ?>

            <?php getTemplate('template.notifications.php'); ?>

            <?php
            if (checkNav('index')) {
                getWidget('widget.being_watched.php');
                getWidget('widget.most_viewed.php');
            }
            ?>

            <?php getTemplate('template.paysite_header.php'); ?>

            <section class="content-sec">
                <div class="wrapper">
                    <div class="row">

                        <?php
                        if (($_GET['controller'] == "index" || checkNav('paysites') || checkNav('channels')) && $_GET['mode'] != "search") {
                            getTemplate('template.sidebar.php');
                        }
                        if ($_GET['controller'] == "members" || ($_GET['mode'] == "search" && $_GET['type'] == "members")) {
                            getTemplate('template.sidebar_members.php');
                        }
                        ?>

                        <!-- main -->
                        <main class="main-col col">
                            <header class="row">
                                
                                <?php $_array = array('pornstar_bio','user_profile','my_profile'); ?>
                                <div class="title-col <?php echo in_array($_GET['controller'], $_array) ? "-model" : "-normal"; ?> col">
                                    <h2>
                                        <?php echo $headertitle; ?>
                                        <?php if (checkNav('index')) { ?> <span class="counter">[<?php echo showVideosCounter(); ?>]</span><?php } ?>
                                    </h2>
                                </div>

                                <?php getWidget('widget.filters.php'); ?>
                                <?php getWidget('widget.header_nav.php'); ?>


                            </header>

                            <?php if ($config['_metatags']['catdesc']) { ?>
                                <div class="row">

                                    <div class="static-col col">
                                        <p><?php echo $config['_metatags']['catdesc']; ?></p>
                                    </div>


                                </div>
                            <?php } ?>

                            <!-- title END -->
                            <div class="row">

                                <?php if (checkNav('pornstars')) { ?>
                                    <?php include($template_path . '/template.pornstar_alphabet.php'); ?>
                                <?php } ?>

                                <!-- HEADER UP -->
                                <!-- HEADER UP END -->