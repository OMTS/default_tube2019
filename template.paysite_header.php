<?php
if ($_GET['controller'] == 'index' && isset($_GET['mode']) && $_GET['mode'] == 'paysites' && is_numeric($_GET['paysite'])) {
    if ($currentLang) {
        $langSelect = ", paysites_languages.data";
        $langJoin = " RIGHT JOIN paysites_languages ON paysites_languages.paysite = paysites.record_num";
        $langWhere = " AND paysites_languages.language = '$currentLang'";
    }

    $getPaysite = dbQuery("SELECT * $langSelect FROM paysites $langJoin WHERE enabled = 1 AND record_num = {$_GET['paysite']} $langWhere", false);
    if (!empty($getPaysite)) {
        $paysite = $getPaysite[0];
        if ($currentLang) {
            $langData = unserialize($paysite['data']);
            $description = $langData['description'];
        } else {
            $description = $paysite['description'];
        }
        ?>
        <?php if (file_exists("$misc_path/paysite{$paysite['record_num']}.jpg") || !empty($description)) { ?>
            <section class="paysite-header">
                <div class="wrapper">
                    <header class="row">

                        <div class="title-col -content col">
                            <h1><?php echo $paysite['name']; ?></h1>
                        </div>


                    </header>

                    <div class="row">

                        <div class="paysite-header-col col">
                            <div class="paysite-avatar">
                                <div class="image">
                                    <?php if (file_exists("$misc_path/paysite{$paysite['record_num']}.jpg")) { ?>
                                        <img src="<?php echo "$misc_url/paysite{$paysite['record_num']}.jpg"; ?>" alt="<?php echo $paysite['name']; ?>">
                                    <?php } else { ?>
                                        <img src="<?php echo $basehttp; ?>/core/images/catdefault.jpg" alt="<?php echo $paysite['name']; ?>" >
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="paysite-info">
                                <div class="paysite-counter -video">
                                    <i class="fas fa-video"></i> <?php echo paysiteVideoNum($paysite['record_num']); ?>
                                </div>

                                <div class="paysite-counter -photo">
                                    <i class="fas fa-image"></i> <?php echo paysiteGalleryNum($paysite['record_num']); ?>
                                </div>

                                <div class="paysite-desc">
                                    <?php echo $description; ?>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </section>
        <?php } ?>
    <?php } ?>
<?php } ?>