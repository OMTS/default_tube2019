<?php
$link = generateUrl('paysite', $row['name'], $row['record_num']);
?>
<!-- item -->
<div class="item-col col -paysite">
    <a href="<?php echo $link; ?>" title="<?php echo htmlentities($row['name'], ENT_QUOTES, 'UTF-8'); ?>">
        <span class="image">
            <?php if (file_exists($misc_path . '/paysite' . $row['record_num'] . '.jpg')) { ?>
                <img src="<?php echo $misc_url; ?>/paysite<?php echo $row['record_num']; ?>.jpg" alt="<?php echo htmlentities($row['name'], ENT_QUOTES, 'UTF-8'); ?>">
            <?php } else { ?>
                <img src="<?php echo $basehttp; ?>/core/images/catdefault.jpg" alt="<?php echo htmlentities($row['name'], ENT_QUOTES, 'UTF-8'); ?>">
            <?php } ?>
        </span>

        <span class="item-info">
            <span class="item-name"><?php echo $row['name']; ?></span>
        </span>
    </a>
</div>
<!-- item END -->