<?php
$sortprefix = "";
if ($_GET['sortby']) {
    $sortprefix = $_GET['sortby'] . "/";
}
?>
<!-- alphabet -->
<div class="alphabet-col col -full">
    <a class="alphabet-letter<?php
    if (!$_GET['letter']) {
        echo ' active';
    }
    ?>" href="<?php echo $basehttp; ?>/models/<?php echo $sortprefix; ?>" ><?php echo _t("All"); ?></a> 
       <?php
       $alphabet = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
       foreach ($alphabet as $i) {
           ?>
        <a class="alphabet-letter<?php
           if ($i == $_GET['letter']) {
               echo ' active';
           }
           ?>" href="<?php echo $basehttp; ?>/models/<?php echo $i; ?>/<?php echo $sortprefix; ?>" ><?php echo ucwords($i); ?></a> 
       <?php } ?>
</div>
<!-- alphabet END -->