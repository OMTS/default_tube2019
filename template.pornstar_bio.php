<div class="content-col col">
    <div class="model-block">
        <div class="model-aside">
            <div class="model-avatar">
                <div class="image">
                    <?php if ($rrow['thumb'] != '') { ?>
                        <img src="<?php echo $basehttp; ?>/media/misc/<?php echo $rrow['thumb']; ?>" />
                    <?php } else { ?>
                        <img src='<?php echo $basehttp; ?>/core/images/avatar.jpg' alt="<?php echo ucwords($rrow['username']); ?>" />
                    <?php } ?>
                </div>
            </div>

            <?php include($basepath . '/includes/rating/templateTh_2019.php'); ?>
        </div>

        <div class="model-info">
            <div class="title-col -sub">
                <h2><?php echo _t("Profile"); ?> <span class="counter">(<?php echo _t("views"); ?> <?php echo $rrow['views']; ?>)</span></h2>
            </div>

            <ul class="model-list">
                <li><span class="sub-label"><?php echo _t("Name"); ?>:</span> <span class="desc"><?php echo $rrow['name']; ?></span></li>
                <li><span class="sub-label"><?php echo _t("Aka"); ?>:</span> <span class="desc"><?php echo $rrow['aka']; ?></span></li>
                <li><span class="sub-label"><?php echo _t("Dob"); ?>:</span> <span class="desc"><?php echo $rrow['dob']; ?></span></li>
                <li><span class="sub-label"><?php echo _t("Height"); ?>:</span> <span class="desc"><?php echo $rrow['height']; ?></span></li>
                <li><span class="sub-label"><?php echo _t("Weight"); ?>:</span> <span class="desc"><?php echo $rrow['weight']; ?></span></li>
                <li><span class="sub-label"><?php echo _t("Measurements"); ?>:</span> <span class="desc"><?php echo $rrow['measurements']; ?></span></li>
                <li><span class="sub-label"><?php echo _t("Hair"); ?>:</span> <span class="desc"><?php echo $rrow['hair']; ?></span></li>
                <li><span class="sub-label"><?php echo _t("Eyes"); ?>:</span> <span class="desc"><?php echo $rrow['eyes']; ?></span></li>
                <li><span class="sub-label"><?php echo _t("Ethnicity"); ?>:</span> <span class="desc"><?php echo $rrow['ethnicity']; ?></span></li>
                <li><span class="sub-label"><?php echo _t("Official site"); ?>:</span> <span class="desc"><a href="<?php echo $rrow['official_site_url']; ?>" target="_blank"> <?php echo $rrow['official_site_name']; ?></a></span></li>
                <li><span class="sub-label"><?php echo _t("Rank"); ?>:</span> <span class="desc"><?php echo $rrow['rank']; ?></span></li>
                <?php $custom = unserialize($rrow['custom']); ?>
                <?php foreach ($custom_pornstar_fields as $k => $v) { ?>
                    <?php if ($custom[$k]) { ?>
                        <li><span class="sub-label"><?php echo $k; ?>:</span> <span class="desc"><?php echo $custom[$k]; ?></span></li>
                    <?php } ?>
                <?php } ?> 
                <li><span class="sub-label"><?php echo _t("Biography"); ?>:</span> <span class="desc"><?php echo $rrow['biography']; ?></span></li>
            </ul>
        </div>
    </div>
</div>

<?php getWidget('widget.ad_content_side.php'); ?>

</div>

<div class="row">

    <div class="model-tabs col">
        <div class="tab-content active show" id="videos" role="tabpanel" aria-labelledby="videos-tab">
            <header class="row">

                <div class="title-col col">
                    <h2><?php echo $rrow['name']; ?>'s <?php echo _t("Videos") ?></h2>
                </div>


            </header>

            <div class="row">

                <?php getModelsContent($rrow['record_num'], 0); ?>


            </div>
        </div>

        <div class="tab-content" id="photos" role="tabpanel" aria-labelledby="photos-tab">
            <header class="row">

                <div class="title-col col">
                    <h2><?php echo $rrow['name']; ?>'s <?php echo _t("Photos") ?></h2>
                </div>


            </header>

            <div class="row">

                <?php getModelsContent($rrow['record_num'], 1); ?>


            </div>
        </div>

        <div class="tab-content" id="wall" role="tabpanel" aria-labelledby="wall-tab">
            <header class="row">

                <div class="title-col col">
                    <h2><?php echo $rrow['name']; ?>'s <?php echo _t("Wall") ?></h2>
                </div>


            </header>

            <div class="row">

                <?php
                $contentID = $rrow['record_num'];
                $commentsType = 1;
                include('widgets/widget.comments.php');
                ?>


            </div>
        </div>
    </div>