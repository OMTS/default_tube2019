<?php
$link = generateUrl('models', $row['name'], $row['record_num']);
?>
<!-- item -->
<div class="item-col col -model">
    <a href="<?php echo $link; ?>" title="<?php echo ucwords($row['name']); ?>">
        <span class="image">
            <?php if ($config['lazyload']) { ?>
                <?php if ($row['thumb'] != '') { ?>
                    <img class="lazy" src="<?php echo $template_url; ?>/images/loader_horizontal.jpg" data-src="<?php echo $basehttp; ?>/media/misc/<?php echo $row['thumb']; ?>"  alt="<?php echo ucwords($row['name']); ?>">
                <?php } else { ?>
                    <img class="lazy" src="<?php echo $template_url; ?>/images/loader_horizontal.jpg" data-src='<?php echo $basehttp; ?>/core/images/catdefault.jpg' alt="<?php echo ucwords($row['name']); ?>">
                <?php } ?>	
            <?php } else { ?>
                <?php if ($row['thumb'] != '') { ?>
                    <img src="<?php echo $basehttp; ?>/media/misc/<?php echo $row['thumb']; ?>"  alt="<?php echo ucwords($row['name']); ?>">
                <?php } else { ?>
                    <img src='<?php echo $basehttp; ?>/core/images/catdefault.jpg' alt="<?php echo ucwords($row['name']); ?>">
                <?php } ?>	
            <?php } ?>
            <span class="item-name"><?php echo ucwords($row['name']); ?></span>
        </span>
    </a>
</div>
<!-- item END -->
