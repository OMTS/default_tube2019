<center>
<? if($insuffientTokens) { ?>
Sorry, you don't have enough tokens to view this video. You currently have <? echo (int)$currentUserTokens; ?> and this video requires <? echo (int)$videoNumTokens; ?>.<br><br>
<a class='btn btn-default btn-sm' href='<? echo $basehttp; ?>/buy-tokens'><? echo _t("Purchase Tokens"); ?></a><br><br>
<? } else { ?>
You are about to purchase access to "<? echo $rrow['title']; ?>" at a cost of <? echo (int)$videoNumTokens; ?> tokens for a period of <? echo datediff(false,time(),time()+$vodRentalLength,true); ?>.<br><br>
<a class='btn btn-default btn-sm' href='<? echo $basehttp; ?>/purchase/<? echo $rrow['record_num']; ?>?confirm=true'><? echo _t("Confirm Purchase"); ?></a><br><br>
<? } ?>
</center>