<!-- aside-main -->
<aside class="aside-main-col col" data-mb="aside" data-opt-filters-on="<?php echo _t("Show filters"); ?>" data-opt-filters-off="<?php echo _t("Hide filters"); ?>">
    <div class="filter-box">
        <div class="filter-header">
            <?php echo _t("Filter Content"); ?>
        </div>

        <div class="filter-content">
            <div class="filter-type">
                <div class="filter-item">
                    <a href="<?php echo $basehttp . "/filter-content/a"; ?>" title="<?php echo _t("All") ?>" class="<?php if ($_SESSION['filterType'] != "S" && $_SESSION['filterType'] != "G" && $_SESSION['filterType'] != "T") { ?>active<?php } ?>"><?php echo _t("All"); ?></a>
                </div>

                <div class="filter-item">
                    <a href="<?php echo $basehttp . "/filter-content/S"; ?>" title="<?php echo _t("Straight"); ?>" class="<?php if ($_SESSION['filterType'] == "S") { ?>active<?php } ?>"><?php echo _t("Straight"); ?></a>
                </div>

                <div class="filter-item">
                    <a href="<?php echo $basehttp . "/filter-content/G"; ?>" title="<?php echo _t("Gay"); ?>" class="<?php if ($_SESSION['filterType'] == "G") { ?>active<?php } ?>"><?php echo _t("Gay"); ?></a>
                </div>

                <div class="filter-item">
                    <a href="<?php echo $basehttp . "/filter-content/T"; ?>" title="<?php echo _t("Shemale"); ?>" class="<?php if ($_SESSION['filterType'] == "T") { ?>active<?php } ?>"><?php echo _t("Shemale"); ?></a>
                </div>
            </div>
        </div>
    </div>

    <?php if (($_GET['mode'] == 'channel' && is_numeric($_GET['channel'])) || ($_GET['mode'] == 'paysites' && is_numeric($_GET['paysite']))) { ?>
        <div class="filter-box">
            <div class="filter-header">
                <?php echo _t("Filter Type"); ?>
            </div>

            <div class="filter-content">
                <div class="filter-type">
                    <div class="filter-item">
                        <a href="<?php echo $basehttp . "/filter/all"; ?>" title="<?php echo _t("All") ?>" class="<?php if (!isset($_SESSION['filterContent']) || $_SESSION['filterContent'] == '') { ?>active<?php } ?>"><?php echo _t("All"); ?></a>
                    </div>

                    <div class="filter-item">
                        <a href="<?php echo $basehttp . "/filter/videos"; ?>" title="<?php echo _t("Videos"); ?>" class="<?php if ($_SESSION['filterContent'] == "videosOnly") { ?>active<?php } ?>"><?php echo _t("Videos"); ?></a>
                    </div>

                    <div class="filter-item">
                        <a href="<?php echo $basehttp . "/filter/photos"; ?>" title="<?php echo _t("Photos"); ?>" class="<?php if ($_SESSION['filterContent'] == "photosOnly") { ?>active<?php } ?>"><?php echo _t("Photos"); ?></a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if ($_GET['mode'] !== 'favorites' && $_GET['mode'] !== 'my_uploads') { ?>
        <div class="filter-box">
            <div class="filter-header">
                <?php echo _t("Categories"); ?>
            </div>

            <div class="filter-content">
                <div class="channels-list -scrollbar">
                    <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                    <div class="viewport">
                        <div class="overview">
                            <?php showChannels3('<li>', '</li>', true, false); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if ($_GET['mode'] !== 'photos' && $_SESSION['filterContent'] !== 'photosOnly' && $_GET['controller'] !== 'channels') { ?>
        <div class="filter-box">
            <div class="filter-header">
                <?php echo _t("Duration"); ?> <span class="sub-label">(<?php echo _t("minutes"); ?>)</span>
            </div>

            <div class="filter-content">
                <div class="duration">
                    <?php
                    $range_from = (!empty($_GET['durationFrom'])) ? (int) $_GET['durationFrom'] / 60 : 0;
                    $range_to = (!empty($_GET['durationTo'])) ? (int) $_GET['durationTo'] / 60 : 50;
                    ?>
                    <input type="text" data-from="<?php echo $range_from; ?>" data-to="<?php echo $range_to; ?>" data-max="50" data-min="0" data-attr-from="durationFrom" data-attr-to="durationTo" data-multiplication="60" id="range_length_filter" name="filter_length" value="" >
                </div>
            </div>
        </div>
    <?php } ?>
</aside>
<!-- aside-main END -->