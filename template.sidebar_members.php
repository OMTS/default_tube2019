<!-- aside-main -->
<aside class="aside-main-col col" data-mb="aside" data-opt-filters-on="<?php echo _t("Show filters"); ?>" data-opt-filters-off="<?php echo _t("Hide filters"); ?>">
    <div class="filter-box">
        <div class="filter-header">
            <?php echo _t("Search Filter"); ?>
        </div>

        <div class="filter-content">
            <div class="users-filters">
                <form action="<?php echo $basehttp ?>/members/" method="post" name="formContact" id="contact-form" class="form-block">
                    <div class="row">

                        <div class="form-item col -full">
                            <label><?php echo _t("Username") ?></label>
                            <?php $ms_name = ($_GET['q'] ? $_GET['q'] : $_SESSION['ms']['name']); ?>
                            <input type="text" placeholder="<?php echo _t("Username"); ?>" value="<?php echo $ms_name; ?>" id="name" name="name">
                        </div>


                    </div>

                    <div class="row">

                        <div class="form-item col -full">
                            <label><?php echo _t("Age") ?></label>
                            <?php
                            $age_from = (!empty($_SESSION['ms']['ageFrom'])) ? (int) $_SESSION['ms']['ageFrom'] : 18;
                            $age_to = (!empty($_SESSION['ms']['ageTo'])) ? (int) $_SESSION['ms']['ageTo'] : 100;
                            ?>
                            <input type="text" data-method="onpost" data-from="<?php echo $age_from; ?>" data-to="<?php echo $age_to; ?>" data-max="98" data-min="18" data-attr-from="ageFrom" data-attr-to="ageTo" data-step="4" id="range_length_filter" name="filter_age" value="" >
                        </div>


                    </div>

                    <div class="row">

                        <div class="form-item col -full">
                            <label><?php echo _t("Location") ?></label>
                            <input type="text" placeholder="<?php echo _t("Location"); ?>" value="<?php echo $_SESSION['ms']['location']; ?>" id="location" name="location">
                        </div>


                    </div>

                    <div class="row">

                        <div class="form-item col -full">
                            <label><?php echo _t("Gender") ?></label>
                            <select name="gender" data-style="btn-selectpicker">
                                <option value="all"><?php echo _t("All") ?></option>
                                <option<?php echo ($_SESSION['ms']['gender'] == 'Male') ? " selected" : ""; ?> value='Male'><?php echo _t("Male") ?></option>
                                <option<?php echo ($_SESSION['ms']['gender'] == 'Female') ? " selected" : ""; ?> value='Female'><?php echo _t("Female") ?></option>
                            </select>
                        </div>


                    </div>

                    <div class="row">

                        <div class="form-item col -full">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="hasAvatar" value='1' <?php echo ($_SESSION[ms][avatar] == 1) ? "checked='checked'" : ""; ?>>
                                    <span class="sub-label"><?php echo _t("With avatars") ?></span>
                                </label>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="hasVideos" value='1' id="checkbox"  <?php echo ($_SESSION[ms][video] == 1) ? "checked='checked'" : ""; ?>>
                                    <span class="sub-label"><?php echo _t("Has uploaded videos"); ?></span>
                                </label>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="hasPhotos" value='1' id="checkbox"  <?php echo ($_SESSION[ms][photo] == 1) ? "checked='checked'" : ""; ?>>
                                    <span class="sub-label"><?php echo _t("Has uploaded photos"); ?></span>
                                </label>
                            </div>
                        </div>


                    </div>

                    <div class="row">

                        <div class="form-item col -actions">
                            <button name="membersFilter" type="submit" class="btn btn-default"><span class="btn-label"><?php echo _t("Search"); ?></span></button>
                            <?php if ($_SESSION['ms']) { ?>
                                <a href="<?php echo $basehttp; ?>/unsetFilters" class="btn btn-dark"><span class="btn-label"><?php echo _t("Reset filters"); ?></span></a>
                            <?php } ?>
                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div>
</aside>
<!-- aside-main END -->
