<!-- alphabet -->
<div class="alphabet-col col col-full">
    <a class="alphabet-letter<?php
    if (!$_GET['letter']) {
        echo ' active';
    }
    ?>" href="/tags<?php echo $sortprefix; ?>" ><?php echo _t("All"); ?></a> 
       <?php
       $alphabet = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
       foreach ($alphabet as $i) {
           ?>
        <a class="alphabet-letter<?php
        if ($i == $_GET['letter']) {
            echo ' active';
        }
        ?>" href="/tags/<?php echo $i; ?><?php echo $sortprefix; ?>" ><?php echo ucwords($i); ?></a> 
       <?php } ?>
</div>

<div class="tags-col col">
    <?php
    if (!is_array($result)) {
        echo _t("Sorry, no tags found!");
    }
//    $result = partition($result, 4);
    foreach ($result as $row) {
        ?>
        <div class="tag-item">
            <a href='<?php echo $basehttp; ?>/search/<?php echo clearString($row['word']); ?>/'>
                <span class="sub-label"><?php echo $row['word']; ?></span>
                <span class="counter">(<?php echo $row['amount']; ?>)</span>
            </a> 
        </div>
        <?php
    }
    ?>
</div>