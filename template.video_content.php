<!-- VIDEO CONTENT -->
<div class="stage">
    <?php
    //check if videoRatio should be used. It should be false only when in VOD Mode user does not have access to video or in Paysite Mode where user is not premium and mode is set to thumbnails (mode 2)
    $showVideoRatio = false;
    if (!$vodMode && $paysiteMode != 2) {
        $showVideoRatio = true;
    } else {
        if ($_SESSION['premium']) {
            $showVideoRatio = true;
        }
        if ($vodMode && billingCheckUserContentAuth($_SESSION['userid'], $rrow['record_num'])) {
            $showVideoRatio = true;
        }
    }
    ?>
    <div class="inner-stage" <?php
    if ($showVideoRatio) {
        echo videoRatio($rrow);
    }
    ?>>
             <?php if (($vodMode && !billingCheckUserContentAuth($_SESSION['userid'], $rrow['record_num'])) || (!$_SESSION['premium'] && $paysiteMode == '2' && !billingCheckUserContentAuth($_SESSION['userid'], $rrow['record_num']))) { ?>
            <center><span style='font-size: 18px; font-weight: bold;'><?php echo _t("You must purchase access to this video in order to view it."); ?></span></center><br>
            <div class="preview-thumbs">
                <?php
                $dirname = str_replace('.flv', '', $rrow['orig_filename']);
                $subdir = $rrow['filename'][0] . '/' . $rrow['filename'][1] . '/' . $rrow['filename'][2] . '/' . $rrow['filename'][3] . '/' . $rrow['filename'][4] . '/';
                $dirname = $subdir . $dirname;
                ?>
                <?php for ($i = 1; $i <= 8; $i++) { ?>

                    <img src="<?php echo $thumb_url; ?>/<?php echo $dirname; ?>/<?php echo $rrow['orig_filename']; ?>-<?php echo $i; ?>.jpg" title="<?php echo $row['title'] . '-' . $i ?>" style='width: 24%'>

                <?php } ?>
                <div class="signup-link" style='text-align: center;'>
                    <br>
                    <?php
                    if ($vodMode) {
                        $purchaseLink = "$basehttp/purchase/$rrow[record_num]";
                    } else {
                        $purchaseLink = "$basehttp/signup";
                    }
                    ?>
                    <a class='btn btn-default btn-sm' href="<?php echo $purchaseLink; ?>"><?php echo _t("Click Here To Purchase Access!"); ?></a><br><br>
                </div>
            </div>
        <?php } else { ?>

            <?php displayPlayerWrapper($rrow, "100%", "100%"); ?>

        <?php } ?>
    </div>
</div>

<?php if (((detectMobile() && $sceneSelectionMobile) || (!detectMobile() && $sceneSelectionDesktop)) && !$rrow['embed'] && !$rrow['vr']) { ?>
    <div class="content-vtt">
        <div class="row vtt-thumbs">
            <?php echo displaySceneSelection(); ?>
        </div>
    </div>
<?php } ?>

<div class="content-info">
    <div class="content-controls">
        <?php include($basepath . '/includes/rating/templateTh_2019.php'); ?>

        <div class="content-tabs">
            <ul class="nav tabs-nav" role="tablist">
                <li>
                    <a class="active" id="about-tab" data-toggle="tab" href="#about" role="tab" aria-controls="about" aria-selected="true"><?php echo _t("About"); ?></a>
                </li>
                <li>
                    <a id="share-tab" data-toggle="tab" href="#share" role="tab" aria-controls="share" aria-selected="false"><?php echo _t("Share"); ?></a>
                </li>
                <?php if (empty($rrow['embed'])) { ?>
                    <li>
                        <a id="download-tab" data-toggle="tab" href="#download" role="tab" aria-controls="download" aria-selected="false"><?php echo _t("Download"); ?></a>
                    </li>
                <?php } ?>
                <?php if ($allowAddFavorites) { ?>
                    <li>
                        <a href="<?php echo $basehttp; ?>/action.php?action=add_favorites&id=<?php echo $rrow['record_num']; ?>" data-mb="modal" data-opt-type="ajax" data-opt-close="<?php echo _t("Close"); ?>" title="<?php echo _t("Add to favorites"); ?>">
                            <i class="icon fas fa-heart"></i>
                            <span class="sub-label"><?php echo _t("Add to favorites"); ?></span>
                        </a>
                    </li>
                <?php } ?>
                <li>
                    <a href="<?php echo $basehttp; ?>/action.php?action=reportVideo&id=<?php echo $rrow['record_num']; ?>" data-mb="modal" data-opt-type="iframe" data-opt-iframe-width="100%"  data-opt-iframe-height="466px" data-toggle="tooltip" title="<?php echo _t("Report"); ?>">
                        <i class="icon fas fa-flag"></i>
                        <span class="sub-label"><?php echo _t("Report"); ?></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="tabs-container">
        <div class="tab-content show active" id="about" role="tabpanel" aria-labelledby="about-tab">
            <?php
            $_description = $rrow['description'];
            $_pornstar = showPornstar($rrow['record_num'], "", "", FALSE);
            $_keywords = buildTags3($rrow['keywords']);
            $_channels = buildChannels3($rrow['record_num']);
            ?>
            <div class="content-base-info">
                <div class="content-submitter">
                    <?php if (!$rrow['submitter'] == 0) { ?>
                        <a href="<?php echo generateUrl('user', $rrow['username'], $rrow['submitter']); ?>" title="<?php echo $rrow['username']; ?>">
                        <?php } ?>
                        <span class="avatar">
                            <?php echo getUserAvatar($rrow['submitter']); ?>
                        </span>
                        <?php if (!$rrow['submitter'] == 0) { ?>
                        </a>
                    <?php } ?>
                    <span class="sub-label">Submitted by</span>
                    <?php if ($rrow['submitter'] == 0) { ?>
                        <?php echo _t("Anonymous") ?>
                    <?php } else { ?>
                        <a href="<?php echo generateUrl('user', $rrow['username'], $rrow['submitter']); ?>" title="<?php echo $rrow['username']; ?>">
                            <?php echo $rrow['username']; ?>
                        </a>
                    <?php } ?>
                </div>

                <div class="info-elem -length">
                    <i class="far fa-clock"></i>
                    <span class="sub-label"><?php echo sec2time($rrow['length']); ?></span>
                </div>

                <div class="info-elem -length">
                    <i class="far fa-eye"></i>
                    <span class="sub-label"><?php echo $rrow['views']; ?></span>
                </div>

                <div class="info-elem -length">
                    <i class="far fa-calendar"></i>
                    <span class="sub-label"><?php echo $rrow['date_added']; ?></span>
                </div>
            </div>

            <?php if (!empty($_description)) { ?>
                <div class="content-desc">
                    <?php echo $_description; ?>
                </div>
            <?php } ?>
            <?php if ($_pornstar != false) { ?>
                <div class="content-links -models">
                    <div class="label">
                        <i class="far fa-star"></i> <?php echo _t("Modles"); ?>:
                    </div>

                    <?php echo $_pornstar; ?>
                </div>
            <?php } ?>
            <?php if (!empty($_keywords)) { ?>
                <div class="content-links -tags">
                    <div class="label">
                        <i class="fas fa-tag"></i> <?php echo _t("Tags"); ?>:
                    </div>

                    <?php echo $_keywords; ?>
                </div>
            <?php } ?>
            <?php if (!empty($_channels)) { ?>
                <div class="content-links -niches">
                    <div class="label">
                        <i class="fas fa-list-ul"></i> <?php echo _t("Categories"); ?>:
                    </div>

                    <?php echo $_channels; ?>
                </div>
            <?php } ?>
        </div>

        <div class="tab-content" id="share" role="tabpanel" aria-labelledby="share-tab">
            <div class="content-share">
                <div class="content-embed">
                    <textarea class="embed-code" readonly>&lt;iframe src='<?php echo $basehttp; ?>/embed/<?php echo $rrow['record_num']; ?>' frameborder='0' height='400' width='600'&gt;&lt;/iframe&gt;&lt;br&gt;&lt;strong&gt;<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>&lt;/strong&gt; - powered by &lt;a href='<?php echo $basehttp; ?>'&gt;<?php echo $sitename; ?>&lt;/a&gt;</textarea>
                    <small>(<?php echo _t("Click in textarea to copy code"); ?>)</small>
                    <span class="copy-info"><?php echo _t("Code copied to clipboard"); ?>.</span>
                </div>

                <div class="content-socials">
                    <ul class="social-list">
                        <li><a data-mb="popup" data-opt-width="500" data-opt-height="400" href="//www.facebook.com/share.php?u=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>&title=<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a data-mb="popup" data-opt-width="500" data-opt-height="400" href="//twitter.com/home?status=<?php echo $basehttp . $_SERVER['REQUEST_URI']; ?>+<?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>" title="<?php echo _t("Share") ?> <?php echo htmlentities($rrow['title'], ENT_QUOTES, 'UTF-8'); ?>"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#" title=""><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#" title=""><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <?php if (empty($rrow['embed'])) { ?>
            <div class="tab-content" id="download" role="tabpanel" aria-labelledby="download-tab">
                <div class="content-download">
                    <div class="download-item -label">
                        <span class="resolution"><?php echo _t("Resolution"); ?></span>
                        <span class="size"><?php echo _t("Size"); ?></span>
                        <span class="download"><?php echo _t("Download"); ?></span>
                    </div>

                    <div class="download-item">
                        <span class="resolution"><?php echo $rrow['movie_height'] !== '0' ? $rrow['movie_height'] . "p" : ""; ?></span>
                        <span class="size"><?php echo filesize_formatted(filesize($video_path . '/' . $rrow['filename'][0] . '/' . $rrow['filename'][1] . '/' . $rrow['filename'][2] . '/' . $rrow['filename'][3] . '/' . $rrow['filename'][4] . '/' . $rrow['filename'])); ?></span>
                        <span class="download"><a href="<?php echo $video_url . '/' . $rrow['filename'][0] . '/' . $rrow['filename'][1] . '/' . $rrow['filename'][2] . '/' . $rrow['filename'][3] . '/' . $rrow['filename'][4] . '/' . $rrow['filename']; ?>" download="<?php echo $rrow["title"]; ?>" class="btn btn-default">Download Now</a></span>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<?php if ($allowDisplayComments) { ?>
    <div class="content-comments">
        <div class="title-col -comments">
            <i class="far fa-comment-alt"></i> <h4><?php echo _t("Comments"); ?></h4>
        </div>

        <?php $contentID = $rrow['record_num']; ?>
        <?php $commentsType = 0; ?>
        <?php include('widgets/widget.comments.php'); ?>
    </div>
<?php } ?>