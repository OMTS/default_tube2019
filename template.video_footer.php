</div>

<?php getWidget('widget.ad_content_side.php'); ?>


</div>
</div>
</section>

<section class="related-sec">
    <div class="wrapper">
        <header class="row">

            <div class="title-col col">
                <h2><?php echo _t("Related Content"); ?></h2>
            </div>


        </header>

        <div class="row">

            <?php showRelated($rrow['related'], 'template.content_item.php', $rrow['record_num'], 15); ?>


        </div>
    </div>
</section>


<?php getWidget('widget.ad_bottom.php'); ?>
<?php getWidget('widget.footer_links.php'); ?>
</section>
<!--[if IE]><script src="<? echo $template_url; ?>/js/ie/ie10fix.js" title="viewport fix"></script><![endif]-->
<!--[if lt IE 9]><script src="<? echo $template_url; ?>/js/ie/ie.min.js"></script><script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script><![endif]-->
</body>
</html>