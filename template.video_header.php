<?php
$colorClass = ($_COOKIE["mbColorScheme"] == 1) ? " inverted-colors" : "";
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7<?php echo $colorClass ?>"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie-7-only<?php echo $colorClass ?>"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie-8-only<?php echo $colorClass ?>"><![endif]-->
<!--[if gte IE 9]><!--> <html class="no-js no-filter<?php echo $colorClass ?>"><!--<![endif]-->
    <head>
        <?php getWidget('widget.header_scripts.php'); ?>
    </head>
    <body class="page-<?php echo bodyClasses(); ?>">
        <section class="page-wrap">
            <?php getTemplate('template.nav.php'); ?>

            <?php getTemplate('template.notifications.php'); ?>

            <section class="content-sec">
                <div class="wrapper">

                    <header class="row">

                        <div class="title-col -content col">
                            <h2><?php echo $rrow['title']; ?></h2>
                        </div>


                    </header>

                    <div class="row">

                        <div class="content-col col">