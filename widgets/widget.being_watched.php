<section class="being-watched-sec">
    <div class="wrapper">
        <div class="row">

            <div class="being-watched-col col">
                <header class="row">

                    <div class="title-col col">
                        <h2><?php echo _t("Being Watched Now"); ?></h2>
                        <a href="<?php echo $basehttp; ?>/videos/" title="<?php echo _t("see more"); ?>"><?php echo _t("see more"); ?></a>
                    </div>

                    <?php getWidget('widget.filter_type.php'); ?>


                </header>

                <div class="row">

                     <?php showBeingWatched('template.content_item.php', 8, 'indexRandom'); ?>


                </div>
            </div>

            <div class="channels-list-col col">
                <div class="channels-list-header">
                    <h2>Categories</h2>
                </div>

                <div class="channels-list -scrollbar">
                    <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                    <div class="viewport">
                        <div class="overview">
                            <?php showChannels3('<li>', '</li>', false, true); ?>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>