<div class="comments-wrapper">
    <div class="comments-load" data-mb="load-comments" data-opt-url="<?php echo $template_url; ?>/template.ajax_comments.php" data-opt-id="<?php echo $contentID; ?>" data-opt-type="<?php echo $commentsType; ?>">

    </div>

    <div class="expand-trigger">
        <a href="#" title="" class="btn btn-header" data-mb="expand" data-opt-limit="4" data-opt-target="comments">
            <span class="sub-label -off"><?php echo _t("Show more") ?></span>
            <span class="sub-label -on"><?php echo _t("Show less") ?></span>
        </a>
    </div>

    <?php if ($allowAddComments) { ?>
        <?php if ($_SESSION['userid']) { ?>
            <div class="comment-alert" data-mb="comment-alert"></div>

            <div class="add-comment">
                <form id="myform" name="comments" class="sendCommentsBox" action="" data-mb="add-comment">
                    <input type='hidden' name='id' id='id' value='<?php echo $contentID; ?>' />
                    <input type='hidden' name='type' id='type' value='<?php echo $commentsType; ?>' />

                    <div class="row">
                        <div class="form-item col -full">
                            <textarea placeholder="<?php echo _t("Write your comment here"); ?>..." id="comment" name="comment"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-item col -captcha">
                            <div class="captcha-wrapper">
                                <img class="captcha-img" src="<?php echo $basehttp; ?>/captcha.php?<?php echo rand(0, 9999); ?>">
                                <input type="text" placeholder="<?php echo _t("Human?") ?>" value="" name="captcha" id="captchaCom" class="captcha-input">
                            </div>
                        </div>

                        <div class="form-item col -actions">
                            <button id="button" name="button" type="submit" class="btn btn-default">
                                <i class="fas fa-plus"></i> Comment
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        <?php } else { ?>
            <div class="add-comment">
                <div class="notification alert-warning" style="margin-bottom:0;">
                    <p><?php echo _t("You must be logged in to post wall comments") ?>. <?php echo _t("Please") ?> <a href='<?php echo $basehttp; ?>/login'><?php echo _t("Login") ?></a> <?php echo _t("or") ?> <a href='<?php echo $basehttp; ?>/signup'><?php echo _t("Signup (free)") ?></a>.</p>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>