<div class="header-filter col">
    <ul class="inline-list">
        <li><a href="<?php echo $basehttp; ?>/filters/all" title="<?php echo _t("All"); ?>"<?php echo (!isset($_SESSION['filters']) || $_SESSION['filters']['all'] === '1') ? ' class="active"' : ""; ?>><?php echo _t("All"); ?></a></li>
        <li><a href="<?php echo $basehttp; ?>/filters/hd" title="<?php echo _t("HD"); ?>"<?php echo (isset($_SESSION['filters']) && $_SESSION['filters']['hd'] === '1') ? ' class="active"' : ""; ?>><?php echo _t("HD"); ?></a></li>
        <li><a href="<?php echo $basehttp; ?>/filters/vr" title="<?php echo _t("VR"); ?>"<?php echo (isset($_SESSION['filters']) && $_SESSION['filters']['vr'] === '1') ? ' class="active"' : ""; ?>><?php echo _t("VR"); ?></a></li>
    </ul>
</div>