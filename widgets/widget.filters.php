<?php

if ((checkNav('videos') || checkNav('most-viewed') || checkNav('top-rated') || checkNav('most-recent') || ($_GET['mode'] == 'channel' && is_numeric($_GET['channel'])) || ($_GET['mode'] == 'paysites' && is_numeric($_GET['paysite']))) && $_SESSION['filterContent'] !== 'photosOnly') {
    getWidget('widget.filter_type.php');
}

if (checkNav('index') || checkNav('most-viewed') || checkNav('top-rated') || checkNav('most-recent')) {
    buildSortFilter('sort', array('photos' => FALSE));
}

$_temp = array('uploads-by-user', 'channel', 'search', 'paysites', 'photos', 'favorites');
if ($_GET[controller] == "index" && !in_array($_GET['mode'], $_temp)) {
    buildSortFilter('time');
}
unset($_temp);

if ($_GET['mode'] == 'channel' && is_numeric($_GET['channel'])) {
    buildSortFilter('channel-paysite', array('type' => 'channel', 'title' => $headertitle, 'id' => $_GET['channel']));
}

if ($_GET['mode'] == 'photos') {
    buildSortFilter('sort', array('photos' => TRUE));
}

if ($_GET['controller'] == 'pornstars') {
    buildSortFilter('pornstars');
}

if ($_GET['mode'] == 'search' && $_GET['type'] != 'members') {
    buildSortFilter('search_content');
}

if ($_GET['mode'] == 'paysites' && is_numeric($_GET['paysite'])) {
    buildSortFilter('channel-paysite', array('type' => 'paysite', 'title' => $name, 'id' => $_GET['paysite']));
}

if ($_GET[controller] == "members" || ($_GET['mode'] == "search" && $_GET['type'] == "members")) {
    buildSortFilter('user');
}
?>