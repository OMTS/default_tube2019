<footer class="footer-sec">
    <div class="wrapper">
        <div class="row">

            <div class="footer-logo-col col">
                <a href="<?php echo $basehttp; ?>" title="<?php echo $sitename; ?>">
                    <img src="<?php echo $template_url; ?>/images/logo.png" title="<?php echo $sitename; ?>">
                </a>
            </div>

            <div class="footer-list-col col">
                <ul class="footer-list">
                    <li><a href="<?php echo $basehttp; ?>/videos/" title="<?php echo _t("Movies"); ?>"><?php echo _t("Movies"); ?></a></li>
                    <li><a href="<?php echo $basehttp; ?>/photos/" title="<?php echo _t("Albums"); ?>"><?php echo _t("Albums"); ?></a></li>
                    <li><a href="<?php echo $basehttp; ?>/channels/" title="<?php echo _t("Categories"); ?>"><?php echo _t("Categories"); ?></a></li>
                    <li><a href="<?php echo $basehttp; ?>/models/" title="<?php echo _t("Models"); ?>"><?php echo _t("Models"); ?></a></li>
                    <li><a href="<?php echo $basehttp; ?>/members/" title="<?php echo _t("Community"); ?>"><?php echo _t("Community"); ?></a></li>
                    <li><a href="<?php echo $basehttp; ?>/paysites/" title="<?php echo _t("Paysites"); ?>"><?php echo _t("Paysites"); ?></a></li>
                    <li><a href="<?php echo $basehttp; ?>/tags" title="<?php echo _t("Tags"); ?>"><?php echo _t("Tags"); ?></a></li>
                </ul>
            </div>

            <div class="footer-list-col col">
                <ul class="footer-list">
                    <li><a href="<?php echo $basehttp; ?>/contact" title="<?php echo _t("Contact With Us"); ?>"><?php echo _t("Contact With Us"); ?></a></li>
                    <li><a href="<?php echo $basehttp; ?>/static/faq" title="<?php echo _t("FAQ"); ?>"><?php echo _t("FAQ"); ?></a></li>
                    <li><a href="<?php echo $basehttp; ?>/static/privacy" title="<?php echo _t("Privacy Policy"); ?>"><?php echo _t("Privacy Policy"); ?></a></li>
                    <li><a href="<?php echo $basehttp; ?>/static/regulations" title="<?php echo _t("Regulations"); ?>"><?php echo _t("Regulations"); ?></a></li>
                </ul>
            </div>


        </div>

        <div class="row">

            <div class="social-col col">
                <ul class="social-list">
                    <li><a href="#" title=""><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="#" title=""><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#" title=""><i class="fab fa-instagram"></i></a></li>
                    <li><a href="#" title=""><i class="fab fa-youtube"></i></a></li>
                </ul>
            </div>


        </div>
    </div>
    
    <?php getWidget('widget.footer_custom.php'); ?>
</footer>

<?php getWidget('widget.footer_scripts.php'); ?>