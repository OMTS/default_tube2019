<link rel="stylesheet" media="screen" href="<?php echo $template_url; ?>/css/overwrite.css">
<link rel="stylesheet" media="screen" href="<?php echo $template_url; ?>/css/custom.css">
<?php if ($_GET['mode'] == 'compose') { ?>
    <link rel="stylesheet" media="screen" href="<?php echo $template_url; ?>/css/autocomplete.min.css">
<?php } ?>
<?php if ($_GET['controller'] == 'upload') { ?>
    <link rel="stylesheet" media="screen" href="<?php echo $basehttp; ?>/core/validation/validationEngine.jquery.css">
    <link rel="stylesheet" media="screen" href="<?php echo $template_url; ?>/js/fine-uploader/fine-uploader-new.min.css">
<?php } ?>

<script type="text/javascript" src="<?php echo $template_url; ?>/js/jQuery_v1.12.4.min.js"></script>
<?php if ($_GET['controller'] == "gallery") { ?>
    <script type="text/javascript" src="<?php echo $template_url; ?>/js/jquery.history.js"></script>
    <script type="text/javascript" src="<?php echo $template_url; ?>/js/lightbox.min.js"></script>
    <script type="text/javascript" src="<?php echo $template_url; ?>/js/owl.carousel.min.js"></script>
<?php } ?>
<?php if ($_GET['mode'] == 'compose') { ?>
    <script type="text/javascript" src="<?php echo $template_url; ?>/js/autocomplete.min.js"></script>
<?php } ?>
<?php if ($_GET['controller'] == 'upload') { ?>
    <script type="text/javascript" src="<?php echo $basehttp; ?>/core/validation/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="<?php echo $basehttp; ?>/core/validation/jquery.validationEngine-en.js"></script>
    <script type="text/javascript" src="<?php echo $template_url; ?>/js/base64.js"></script>
    <script type="text/javascript" src="<?php echo $template_url; ?>/js/fine-uploader/jquery.fine-uploader.min.js"></script>
    <script type="text/template" id="qq-template">
        <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Drop files here">
        <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
        </div>
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
        <span class="qq-upload-drop-area-text-selector"></span>
        </div>
        <div class="qq-upload-button-selector qq-upload-button">
        <div>Upload a file</div>
        </div>
        <span class="qq-drop-processing-selector qq-drop-processing">
        <span>Processing dropped files...</span>
        <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
        </span>
        <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
        <li>
        <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
        <div class="qq-progress-bar-container-selector qq-progress-bar-container">
        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
        </div>
        <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
        <div class="qq-thumbnail-wrapper">
        <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
        </div>
        <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
        <button type="button" class="qq-upload-retry-selector qq-upload-retry">
        <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
        Retry
        </button>

        <div class="qq-file-info">
        <div class="qq-file-name">
        <span class="qq-upload-file-selector qq-upload-file"></span>
        <span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon" aria-label="Edit filename"></span>
        </div>
        <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
        <span class="qq-upload-size-selector qq-upload-size"></span>
        <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
        <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
        </button>
        <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
        <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
        </button>
        <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
        <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
        </button>
        </div>
        </li>
        </ul>

        <dialog class="qq-alert-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <div class="qq-dialog-buttons">
        <button type="button" class="qq-cancel-button-selector">Close</button>
        </div>
        </dialog>

        <dialog class="qq-confirm-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <div class="qq-dialog-buttons">
        <button type="button" class="qq-cancel-button-selector">No</button>
        <button type="button" class="qq-ok-button-selector">Yes</button>
        </div>
        </dialog>

        <dialog class="qq-prompt-dialog-selector">
        <div class="qq-dialog-message-selector"></div>
        <input type="text">
        <div class="qq-dialog-buttons">
        <button type="button" class="qq-cancel-button-selector">Cancel</button>
        <button type="button" class="qq-ok-button-selector">Ok</button>
        </div>
        </dialog>
        </div>
    </script>
<?php } ?>
<script type="text/javascript" src="<?php echo $template_url; ?>/js/bootstrap.min.js"></script>
<?php if (($_GET['controller'] == 'index' && isset($_GET['mode']) && $_GET['mode'] !== 'photos' && $_GET['mode'] !== 'search') || ($_GET['controller'] == 'index' && isset($_GET['page']))) { ?>
    <script type="text/javascript" src="<?php echo $template_url; ?>/js/ion.rangeSlider.min.js"></script>
<?php } ?>
<script type="text/javascript" src="<?php echo $template_url; ?>/js/lazyload.min.js"></script>
<script type="text/javascript" src="<?php echo $template_url; ?>/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo $template_url; ?>/js/jquery.tinyscrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo $template_url; ?>/js/img2svg.js"></script>
<script type="text/javascript" src="<?php echo $template_url; ?>/js/functions.js"></script>
<script type="text/javascript" src="<?php echo $basehttp; ?>/core/js/Tube.js"></script>

<?php if ($_GET['mode'] == 'favorites') { ?>
    <script>
    $('.add-to-fav').click(function () {
        $(this).parents('.item-col').fadeOut("slow");
    });
    </script>
<?php } ?>
<?php if ($_GET['controller'] == 'my_friends') { ?>
    <script type="text/javascript">
        function deleteFriend(id) {
            $("#friend" + id).fadeOut("slow");
            $.get('<?php echo $basehttp; ?>/includes/ajax.delete_friend.php?id=' + id);
        }
        function deleteFriendApproved(id) {
            $("#friend" + id).fadeOut("slow");
            $.get('<?php echo $basehttp; ?>/includes/ajax.delete_friend.php?approved=1&id=' + id);
        }
        function approveFriend(id) {
            $("#friend" + id).fadeOut("slow");
            $.get('<?php echo $basehttp; ?>/includes/ajax.approve_friend.php?id=' + id);
        }
        $(document).ready(function () {
            $('#profile h4').click(function () {
                $(this).parent().children('.hidden').slideToggle('fast');
                if ($(this).children('.more').hasClass('off')) {
                    $(this).children('.more').removeClass('off').addClass('on');
                } else {
                    $(this).children('.more').removeClass('on').addClass('off');
                }
            });
        });
    </script>
<?php } ?>
<?php if ($_GET['controller'] == 'upload') { ?>
    <script type="text/javascript">
        function sendForm() {
            $.post('<?php echo $basehttp; ?>/includes/ajax.captcha.php', {
                post: $('#CaptchaForm').serialize()
            }, function (data) {
                data.message = $.base64.decode(data.message);
                if (data.error == 'true') {
                    $('#CaptchaForm .ajaxInfo').html(data.message);
                } else {
                    $('#CaptchaForm .ajaxInfo').html(data.message);
                    $('#hidFileID').val(data.videoid);
                    $('#CaptchaForm').hide();
                    $('#formUpload').show();
                }
            }, 'json');
        }
        $(document).ready(function () {
            $('#CaptchaForm').find('input').each(function () {
                $(this).keypress(function (e) {
                    if (e.which == 13) {
                        sendForm();
                    }
                });
            });
            $('#btnCaptcha').bind('click', function () {
                sendForm();
                return false;
            });
            jQuery("#formUpload").validationEngine();
            $('#fine-uploader').fineUploader({
                multiple: <?php echo $multiple; ?>,
                maxConnections: 1,
                debug: true,
                maxChunkSize: 10000000,
                disableCancelForFormUploads: true,
                request: {
                    endpoint: '<?php echo $basehttp . "/includes/uploader/upload_ajax.php"; ?>',
                    params: {
                        userId: '<?php echo $_SESSION['userid']; ?>',
                        optionUpload: '<?php echo $optionUpload; ?>',
                        token: '<?php echo $_SESSION['token']; ?>',
                        allowedExtensions: <?php echo $allowedExtensions; ?>,
                        acceptFiles: '<?php echo $acceptFiles; ?>',
                        sizeLimit: <?php echo $sizeLimit; ?>
                    }
                },
                validation: {
                    allowedExtensions: <?php echo $allowedExtensions; ?>,
                    acceptFiles: '<?php echo $acceptFiles; ?>',
                    sizeLimit: <?php echo $sizeLimit; ?>
                }
            }).on('complete', function (event, id, filename, responseJSON) {
                if (responseJSON.success) {
                    $('#uploadStatus').html(responseJSON.message);
                } else {
                    $('#uploadStatus').html(responseJSON.message);
                }

            });
        });
    </script>
<?php } ?>
