<?php if ($_GET['controller'] == 'pornstar_bio' || $_GET['controller'] == 'user_profile' || $_GET['controller'] == 'my_profile') { ?>
    <div class="<?php echo ($_GET['controller'] == 'user_profile' || $_GET['controller'] == 'my_profile') ? "user" : "model" ?>-nav-col col">
        <ul class="tabs-nav nav">
            <li>
                <a class="active show" id="videos-tab" data-toggle="tab" href="#videos" role="tab" aria-controls="videos" aria-selected="true">
                    <i class="fas fa-video"></i>
                    <span class="sub-label"><?php echo _t("Videos"); ?></span>
                </a>
            </li>
            <li>
                <a id="photos-tab" data-toggle="tab" href="#photos" role="tab" aria-controls="photos" aria-selected="false">
                    <i class="fas fa-image"></i>
                    <span class="sub-label"><?php echo _t("Photos"); ?></span>
                </a>
            </li>
            <li>
                <a id="wall-tab" data-toggle="tab" href="#wall" role="tab" aria-controls="wall" aria-selected="false">
                    <i class="fas fa-align-justify"></i>
                    <span class="sub-label"><?php echo _t("Wall"); ?></span>
                </a>
            </li>
            <?php if ($_GET['controller'] == 'user_profile' || $_GET['controller'] == 'my_profile') { ?>
                <li>
                    <a id="friends-tab" data-toggle="tab" href="#friends" role="tab" aria-controls="friends" aria-selected="false">
                        <i class="fas fa-user-friends"></i>
                        <span class="sub-label"><?php echo _t("Friends"); ?></span>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>
<?php if ($_GET['controller'] == 'my_friends') { ?>
    <div class="user-nav-col col">
        <ul class="tabs-nav nav">
            <li>
                <a class="active show" id="new-tab" data-toggle="tab" href="#new" role="tab" aria-controls="new" aria-selected="true">
                    <i class="fas fa-envelope-open"></i>
                    <span class="sub-label"><?php echo _t("New invitations"); ?></span>
                </a>
            </li>
            <li>
                <a id="friends-tab" data-toggle="tab" href="#friends" role="tab" aria-controls="friends" aria-selected="false">
                    <i class="fas fa-user-friends"></i>
                    <span class="sub-label"><?php echo _t("My Friends"); ?></span>
                </a>
            </li>
            <li>
                <a id="sent-tab" data-toggle="tab" href="#sent" role="tab" aria-controls="sent" aria-selected="false">
                    <i class="fas fa-share-square"></i>
                    <span class="sub-label"><?php echo _t("Sent Invitations"); ?></span>
                </a>
            </li>
        </ul>
    </div>
<?php } ?>
