<section class="most-viewed-sec -content-aff">
    <div class="wrapper">
        <header class="row">

            <div class="title-col col">
                <h2><?php echo _t("Most Viewed Videos"); ?></h2>
                <a href="<?php echo $basehttp; ?>/most-viewed/" title="<?php echo _t("see more"); ?>"><?php echo _t("see more"); ?></a>
            </div>


        </header>

        <div class="row">

            <?php getWidget('widget.ad_list.php'); ?>

            <?php _showMostViewed('template.content_item.php', 8); ?>


        </div>
    </div>
</section>