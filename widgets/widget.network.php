<section class="network-sec">
    <div class="wrapper">
        <div class="row">
            <!-- network -->
            <div class="network-col col">
                <div class="network-inner-col inner-col">
                    <span class="sub-label"><?php echo _t("Network") ?>:</span>
                    <a href="https://www.mechbunny.com/" title="Mechbunny">Mechbunny</a>
                    <a href="http://tube.mechbunny.com/" title="Mechbunny Tube Script">Mechbunny Tube Script</a>
                    <a href="http://affil.mechbunny.com/" title="Mechbunny Affil Script">Mechbunny Affil Script</a>
                    <a href="http://www.mbtemplates.com/" title="Mechbunny Scripts Teplates">Mechbunny Scripts Templates</a>
                    <a href="#" data-mb="expand-network" title="<?php echo _t("More") ?>"><?php echo _t("More") ?></a>
                </div>
            </div>
            <!-- network END -->
        </div>
    </div>
</section>